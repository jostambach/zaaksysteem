#!/usr/bin/env perl
use warnings;
use strict;

use FindBin;
use lib "$FindBin::Bin/../lib";

use Zaaksysteem::LegacyPublish;
use Getopt::Long;
use Pod::Usage;

my %opt = (
    help     => 0,
    protocol => 'scp'
);

# redirect all error output to stdout, because this script will be called
# from another perl process
*STDERR = *STDOUT;

{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%opt, qw(
                help
                username=s
                password=s
                hostname=s
                port=i
                src_dir=s
                dst_dir=s
                notify_url=s
                notify_wait=s
                notify_filename=s
                csv_filename=s
                protocol=s
                )
        );
    };
    if (!$ok) {
        pod2usage(1) ;
    }
}

pod2usage(0) if ($opt{help});

foreach (qw(username password hostname src_dir)) {
    my $ok = 1;
    unless ($opt{$_}) {
        warn "Missing required option '$_'";
        $ok = 0;
    }
    pod2usage(1) if (!$ok);
}

delete $opt{help};

my $publisher = Zaaksysteem::LegacyPublish->new(%opt);
$publisher->run();

1;

__END__

=head1 NAME

legacy_publish.pl - A rewrite of the legacy publish script

=head1 SYNOPSIS

legacy_publish.pl [ OPTIONS ]

=head1 OPTIONS

=over

=item * --help

This help

=item * --username

The username for the remote service. Required.

=item * --password

The password for the remote service. Required.

=item * --hostname

The hostname for the remote service. Required.

=item * --port

The port for the remote service. Required.

=item * --protocol

The protocol used for the remote service. Currently supports C<ftp> and C<scp>. Defaults to C<scp>.

=item * --src_dir

The source dir where the files are which you want to transfer. Required.

=item * --dst_dir

The destination dir on the remote service Required.

=item * --notify_url

The URL you want to ping once the file transfer has completed. Optional.

=item * --notify_wait

The time you wait before sending the notify to the URL. Defaults to 2 seconds.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
