// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import assign from 'lodash/assign';
import zsDashboardWidgetExternalUrlForm from './zsDashboardWidgetExternalUrlForm';
import zsDashboardWidgetExternalUrlResult from './zsDashboardWidgetExternalUrlResult';
import zsDashboardWidgetExternalUrlBlocked from './zsDashboardWidgetExternalUrlBlocked';
import sessionServiceModule from '../../../../../../../shared/user/sessionService';
import template from './template.html';
import './styles.scss';

const isWhitelisted = (url, whitelist) =>
  whitelist.find((allowedDomain) => url.includes(allowedDomain));

export default angular
  .module(
    'Zaaksysteem.intern.home.zsDashboard.zsDashboardWidget.types.zsDashboardWidgetExternalUrl',
    [
      zsDashboardWidgetExternalUrlForm,
      zsDashboardWidgetExternalUrlResult,
      zsDashboardWidgetExternalUrlBlocked,
      sessionServiceModule,
    ]
  )
  .directive('zsDashboardWidgetExternalUrl', [
    () => {
      return {
        restrict: 'E',
        scope: {
          onDataChange: '&',
          widgetData: '&',
          widgetTitle: '&',
          isWidgetLoading: '&',
          settings: '&',
        },
        template,
        bindToController: true,
        controller: [
          '$rootScope',
          'sessionService',
          function (scope, sessionService) {
            const userResource = sessionService.createResource(scope);

            const getAllWhitelistDomains = () => {
              const dashboardXssUri = userResource.data().instance.configurable
                .dashboard_ui_xss_uri;
              const whitelist =
                typeof dashboardXssUri === 'string'
                  ? dashboardXssUri.split(' ')
                  : dashboardXssUri || [];

              const whitelistWithTrailingSlash = whitelist.map(
                (domain) =>
                  domain + (domain.charAt(domain.length - 1) === '/' ? '' : '/')
              );

              return whitelistWithTrailingSlash;
            };

            let ctrl = this;

            ctrl.getWhitelistDomains = () => {
              const domains = getAllWhitelistDomains();
              return domains.length > 8
                ? domains.slice(0, 8).concat('...')
                : domains;
            };

            ctrl.getState = () => {
              let data = ctrl.widgetData(),
                state;

              if (
                data &&
                data.url &&
                isWhitelisted(data.url, getAllWhitelistDomains())
              ) {
                state = 'display';
              } else if (data && data.url) {
                state = 'notallowed';
              } else {
                state = 'form';
              }
              return state;
            };

            ctrl.handleEnterUrl = (data) => {
              ctrl.onDataChange({
                $newData: assign({}, ctrl.widgetData(), {
                  url: data.url,
                  title: data.title,
                }),
              });
            };

            ctrl.getWidgetTitle = (getter) => {
              ctrl.widgetTitle({ $getter: getter });
            };

            return ctrl;
          },
        ],
        controllerAs: 'zsDashboardWidgetExternalUrl',
      };
    },
  ]).name;
