// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import get from 'lodash/get';
import './styles.scss';
import template from './template.html';

export default angular
  .module('zsDashboardWidgetExternalUrlResult', [])
  .directive('zsDashboardWidgetExternalUrlResult', [
    () => {
      return {
        restrict: 'E',
        scope: {
          widgetData: '&',
          widgetTitle: '&',
          widgetLoading: '&',
          onDataChange: '&',
        },
        template,
        bindToController: true,
        controller: [
          '$element',
          function ($element) {
            let ctrl = this;
            const iframeHtml = `<iframe class="widget-external-url-result" src="${get(
              ctrl.widgetData(),
              'url'
            )}"></iframe>`;

            $element[0].querySelector(
              '.iframeplaceholder'
            ).innerHTML = iframeHtml;

            ctrl.widgetTitle({
              $getter: () => get(ctrl.widgetData(), 'title') || '',
            });

            ctrl.widgetLoading({
              $getter: false,
            });

            return ctrl;
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
