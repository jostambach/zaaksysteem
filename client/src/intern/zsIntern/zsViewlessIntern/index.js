// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './../template.html';
import controller from './../controller';

export default angular
  .module('zsViewlessIntern', [controller])
  .directive('zsViewlessIntern', [
    () => {
      let tpl = template.replace(/<ui-view>.*?<\/ui-view>/, '');

      return {
        restrict: 'E',
        template: tpl,
        scope: {
          onOpen: '&',
          onClose: '&',
        },
        bindToController: true,
        controller,
        controllerAs: 'vm',
      };
    },
  ]).name;
