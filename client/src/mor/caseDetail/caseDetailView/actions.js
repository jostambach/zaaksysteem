// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import mutationServiceModule from '../../../shared/api/resource/mutationService';
import propCheck from '../../../shared/util/propCheck';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import configServiceModule from '../../shared/configService';

export default angular
  .module('Zaaksysteem.mor.caseActions', [
    mutationServiceModule,
    snackbarServiceModule,
    configServiceModule,
  ])
  .run([
    'snackbarService',
    'mutationService',
    'configService',
    (snackbarService, mutationService, configService) => {
      mutationService.register({
        type: 'MOR_TAKE_CASE',
        request: (mutationData) => {
          propCheck.throw(
            propCheck.shape({
              caseRef: propCheck.string,
              userUuid: propCheck.string,
            }),
            mutationData
          );

          snackbarService.info('U heeft de melding in behandeling genomen.');

          return {
            url: `/api/v1/case/${mutationData.caseRef}/take`,
            headers: {
              'API-Interface-Id': configService.getInterfaceId(),
            },
          };
        },
        reduce: (data, mutationData) => {
          return data.map((proposal) => {
            if (proposal.reference === mutationData.caseRef) {
              return proposal.merge(
                {
                  instance: {
                    'assignee.reference': mutationData.userUuid,
                  },
                },
                { deep: true }
              );
            }

            return proposal;
          });
        },
        error: (/*data*/) => {
          snackbarService.error(
            'Er ging iets mis bij het in behandeling nemen van de zaak. Neem contact op met uw beheerder voor meer informatie.'
          );
        },
      });
    },
  ]).name;
