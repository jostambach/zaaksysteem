// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';
import mapKeys from 'lodash/mapKeys';
import startsWith from 'lodash/startsWith';
import pickBy from 'lodash/pickBy';

export default (valueKey, val, idKey = 'id') => {
  let idVal = get(val, `case.${valueKey}.${idKey}`);

  if (idVal === undefined || idVal === null) {
    return null;
  }

  return {
    instance: mapKeys(
      pickBy(val, (value, key) => startsWith(key, `case.${valueKey}`)),
      (value, key) => {
        return startsWith(key, `case.${valueKey}.`)
          ? key.replace(`case.${valueKey}.`, '')
          : key.replace('case.', '');
      }
    ),
  };
};
