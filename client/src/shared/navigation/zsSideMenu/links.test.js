// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { containsOneOf } from './links';

describe('The `containsOneOf` function', () => {
  test('returns `true` if a single option is found', () => {
    const actual = containsOneOf(['foo', 'bar', 'quux'], ['bar']);
    const expected = true;

    expect(actual).toBe(expected);
  });

  test('returns `true` if a single option is found', () => {
    const actual = containsOneOf(['foo', 'bar', 'quux'], ['bar', 'foo']);
    const expected = true;

    expect(actual).toBe(expected);
  });

  test('returns `false` if no option is found', () => {
    const actual = containsOneOf(['foo', 'bar', 'quux'], ['baz']);
    const expected = false;

    expect(actual).toBe(expected);
  });
});
