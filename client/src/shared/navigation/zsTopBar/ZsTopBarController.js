// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import oneWayBind from './../../util/oneWayBind';
import escapeString from 'lodash/escape';

export default class ZsTopBarController {
  static get $inject() {
    return [
      '$scope',
      '$state',
      '$sce',
      '$compile',
      'viewTitle',
      'rwdService',
      'HOME',
    ];
  }

  constructor(scope, $state, $sce, $compile, viewTitle, rwdService, HOME) {
    const ctrl = this;
    let pageTitle;
    let readablePageTitle;

    ctrl.isSpotEnlighterActive = oneWayBind();

    ctrl.isExpanded = ctrl.isSpotEnlighterActive;

    ctrl.getPageTitle = () => pageTitle;

    ctrl.getReadablePageTitle = () => readablePageTitle;

    ctrl.getHome = () => HOME;

    ctrl.isSideMenuActive = () => {
      oneWayBind();
    };

    ctrl.isHome = () => {
      return ctrl.useLocation()
        ? !$state.current.name || $state.current.name === 'home'
        : false;
    };

    ctrl.isDashboardLinkVisible = () =>
      ctrl.isHome() || !rwdService.isActive('small-and-down');

    ctrl.isAllowed = () => ctrl.allowSearch();

    scope.$watch(viewTitle.get, (title) => {
      // viewTitle.get is overwritten from frontend/index.js
      // client output is an object with properties mainTitle and a subTitle which may be null
      // frontend output is a string

      if (title) {
        const { mainTitle, subTitle } = title;

        readablePageTitle = `${mainTitle} ${subTitle ? subTitle : ''}`;

        $compile(`
          <div class="top-bar-current-location-content">
            <span class="top-bar-current-location-main-title">${escapeString(
              mainTitle
            )}</span>
            ${
              subTitle
                ? `<span class="top-bar-current-location-sub-title">${escapeString(
                    subTitle
                  )}</span>`
                : ''
            }
          </div>
          `)(scope, (clonedElement) => {
          scope.$$postDigest(() => {
            pageTitle = $sce.trustAsHtml(clonedElement[0].outerHTML);
          });
        });
      }
    });
  }
}
