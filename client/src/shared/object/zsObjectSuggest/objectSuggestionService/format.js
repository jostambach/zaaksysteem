// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import assign from 'lodash/assign';
import { addressRowFactory } from '../../../util/address/format';

const foreignAddressKeys = [
  'adres_buitenland1',
  'adres_buitenland2',
  'adres_buitenland3',
];

const localAddressDictionary = {
  street: 'straatnaam',
  street_number: 'huisnummer',
  street_number_letter: 'huisletter',
  street_number_suffix: 'huisnummertoevoeging',
  zipcode: 'postcode',
  city: 'woonplaats',
};

export const getPersonAddressRows = addressRowFactory(
  foreignAddressKeys,
  localAddressDictionary
);

const getCompanyKey = (key) => ['vestiging', key].join('_');

export const getCompanyAddressRows = addressRowFactory(
  foreignAddressKeys.map((key) => getCompanyKey(key)),
  Object.keys(localAddressDictionary).reduce(
    (object, key) =>
      assign(object, {
        [key]: getCompanyKey(localAddressDictionary[key]),
      }),
    {}
  )
);
