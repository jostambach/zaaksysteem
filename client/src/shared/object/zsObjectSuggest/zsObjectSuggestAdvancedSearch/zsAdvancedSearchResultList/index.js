// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import zsTableModule from './../../../../ui/zsTable';
import template from './template.html';

export default angular
  .module('zsAdvancedSearchResultList', [zsTableModule])
  .directive('zsAdvancedSearchResultList', [
    () => {
      return {
        restrict: 'E',
        template,
        scope: {
          results: '&',
          columns: '&',
          onSelect: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this;

            ctrl.getColumns = ctrl.columns;
            ctrl.getRows = ctrl.results;

            ctrl.handleRowClick = (item) => {
              ctrl.onSelect({ $object: item });
            };
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
