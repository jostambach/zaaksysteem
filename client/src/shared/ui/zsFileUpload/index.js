// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import ocLazyLoadModule from 'oclazyload/dist/ocLazyLoad';
import assign from 'lodash/assign';
import without from 'lodash/without';
import template from './template.html';
import shortid from 'shortid';
import './styles.scss';

export default angular
  .module('zsFileUpload', [ocLazyLoadModule])
  .directive('zsFileUpload', [
    '$rootScope',
    '$compile',
    '$injector',
    '$q',
    '$ocLazyLoad',
    'snackbarService',
    ($rootScope, $compile, $injector, $q, $ocLazyLoad) => {
      let deferred = $q.defer(),
        jsPromise = deferred.promise;

      require(['ng-file-upload'], (name) => {
        $rootScope.$evalAsync(() => {
          $ocLazyLoad
            .load({
              name,
            })
            .then(() => {
              deferred.resolve($compile(template));
            })
            .catch(deferred.reject);
        });
      });

      return {
        restrict: 'E',
        scope: {
          target: '&',
          data: '&',
          onUpload: '&',
          onProgress: '&',
          onError: '&',
          onComplete: '&',
          onBatchComplete: '&',
          onBatchStart: '&',
          onBatchEnd: '&',
          onBatchError: '&',
          onEnd: '&',
          allowDrop: '&',
          allowSelect: '&',
          disabled: '&',
          fileLimit: '&',
          fileSizeLimit: '&',
          captureMode: '&',
          accept: '&',
          pattern: '&',
        },
        bindToController: true,
        controller: [
          '$scope',
          '$element',
          function (scope, element) {
            let ctrl = this,
              uploadService,
              currentUploads = [],
              buttonName = shortid();

            let uploadFiles = (files) => {
              let uploads = (files || []).map((file) => {
                let locals = { $file: file },
                  target = ctrl.target(locals),
                  data = ctrl.data(locals) || { file },
                  promise,
                  upload = {};

                promise = ctrl.onUpload({
                  $file: file,
                  $target: target,
                  $data: data,
                  $upload: uploadService,
                });

                if (!promise) {
                  promise = uploadService.upload({
                    url: target,
                    data,
                  });
                }

                assign(upload, {
                  name: file.name,
                  promise,
                  progress: 0,
                  percentage: 0,
                  bytesLoaded: 0,
                  bytesTotal: NaN,
                  status: 'progress',
                });

                promise
                  .then(
                    (response) => {
                      assign(upload, {
                        bytesLoaded: upload.bytesTotal,
                        progress: 1,
                        percentage: 100,
                        status: 'completed',
                      });

                      ctrl.onComplete({
                        $file: file,
                        $data: response.data,
                      });
                    },
                    (err) => {
                      assign(upload, {
                        bytesLoaded: upload.bytesTotal,
                        progress: 1,
                        percentage: 100,
                        status: 'error',
                      });

                      ctrl.onError({
                        $file: file,
                        $error: err,
                      });
                    },
                    (evt) => {
                      let bytesLoaded = evt.loaded || 0,
                        bytesTotal = evt.total || 0,
                        progress = bytesLoaded / bytesTotal,
                        percentage = Math.round(progress * 100);

                      assign(upload, {
                        bytesTotal,
                        bytesLoaded,
                        progress,
                        percentage,
                      });

                      ctrl.onProgress({
                        $file: file,
                        $progress: progress,
                        $percentage: percentage,
                        $bytesLoaded: bytesLoaded,
                        $bytesTotal: bytesTotal,
                      });
                    }
                  )
                  .finally(() => {
                    currentUploads = without(currentUploads, upload);

                    ctrl.onEnd({
                      $file: file,
                      status: upload.status,
                    });
                  });

                return upload;
              });

              currentUploads = currentUploads.concat(uploads);

              ctrl.onBatchStart({ $uploads: uploads });

              $q.all(uploads.map((upload) => upload.promise))
                .then(() => {
                  ctrl.onBatchComplete({ $uploads: uploads });
                })
                .catch(() => {
                  ctrl.onBatchError({ $uploads: uploads });
                })
                .finally(() => {
                  ctrl.onBatchEnd({ $uploads: uploads });
                });

              return uploads;
            };

            ctrl.handleFileSelect = uploadFiles;

            ctrl.handleFileDrop = uploadFiles;

            ctrl.handleFileDrag = (/*isDragging, className, event*/) => {};

            ctrl.isDisabled = ctrl.disabled;

            ctrl.isSelectDisabled = () => !ctrl.allowSelect();

            ctrl.isDropDisabled = () => !ctrl.allowDrop();

            ctrl.allowMultiple = () => ctrl.fileLimit() !== 1;

            ctrl.getCaptureMode = ctrl.captureMode;

            ctrl.getAccept = ctrl.accept;

            ctrl.getFileLimit = ctrl.fileLimit;

            ctrl.getMinSize = () => 0;

            ctrl.getMaxSize = ctrl.fileSizeLimit;

            ctrl.getFilenamePattern = ctrl.pattern;

            ctrl.getCurrentUploads = () => currentUploads;

            jsPromise.then((compiler) => {
              uploadService = $injector.get('Upload');

              compiler(scope, (clonedElement) => {
                // add name to element so we can find the input[type=file] element
                // for e2e testing purposes
                clonedElement.find('button').attr('name', buttonName);

                element.append(clonedElement);
              });
            });
          },
        ],
        controllerAs: 'vm',
      };
    },
  ]).name;
