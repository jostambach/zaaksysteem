// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import composedReducerModule from './../../../api/resource/composedReducer';
import zsKeyboardNavigableListModule from './../../zsKeyboardNavigableList';
import controller from './SpotEnlighterSuggestionListController';
import template from './template';

export default angular
  .module('shared.ui.zsSpotEnlighter.zsSpotEnlighterSuggestionList', [
    zsKeyboardNavigableListModule,
    composedReducerModule,
  ])
  .component('zsSpotEnlighterSuggestionList', {
    bindings: {
      keyInputDelegate: '&',
      suggestions: '&',
      onSelect: '&',
      label: '@',
      objectType: '&',
      externalSearchSettings: '&',
      externalSearchResultsCount: '&',
      onObjectTypeChange: '&',
      onChangeQuery: '&',
    },
    controller,
    template,
  }).name;
