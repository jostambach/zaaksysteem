// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default function padLeft(input, pattern) {
  [input, pattern].forEach((param) => {
    const type = typeof param;

    if (type !== 'string') {
      throw new TypeError(`expected ${type} to be a string`);
    }
  });

  if (input.length < pattern.length) {
    const end = pattern.length - input.length;
    const prefix = pattern.slice(0, end);

    return `${prefix}${input}`;
  }

  return input;
}
