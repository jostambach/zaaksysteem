// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default ($interpolate, scope, element, attrs) => {
  let name = $interpolate(attrs.uiView || attrs.name || '')(scope),
    inherited = element.inheritedData('$uiView');

  return name.indexOf('@') >= 0
    ? name
    : `${name}@${inherited ? inherited.state.name : ''}`;
};
