// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import { Promise } from 'es6-promise';
import snackbarServiceModule from './../../ui/zsSnackbar/snackbarService';
import once from 'lodash/once';

const FILENAME = 'service-worker.js';

export default angular
  .module('shared.util.serviceworker', [snackbarServiceModule])
  .factory('serviceWorker', [
    '$window',
    '$rootScope',
    'snackbarService',
    ($window, $rootScope, snackbarService) => {
      return (scope = './') => {
        let serviceWorker = {},
          supported =
            'serviceWorker' in navigator &&
            (window.location.protocol === 'https:' ||
              window.location.hostname === 'localhost' ||
              window.location.hostname.indexOf('127.') === 0),
          registrationPromise,
          enabled;

        let register = () => {
          registrationPromise = navigator.serviceWorker.register(
            scope + FILENAME,
            {
              scope,
            }
          );

          return registrationPromise;
        };

        let emitNewVersion = () => {
          snackbarService.info(
            'Er is een nieuwe versie van Zaaksysteem beschikbaar.',
            {
              timeout: 0,
              actions: [
                {
                  id: 'refresh',
                  label: 'Herladen',
                  click: () => {
                    $window.location.reload();
                  },
                },
              ],
            }
          );
        };

        let getRegistration = () => navigator.serviceWorker.getRegistration();

        serviceWorker.isEnabled = () => {
          return getRegistration(scope).then((registration) => {
            return !!registration;
          });
        };

        serviceWorker.enable = () => {
          return register().catch((err) => {
            console.error('Error installing ServiceWorker', err);
          });
        };

        serviceWorker.disable = () => {
          let promises = [];

          if (enabled === false) {
            return serviceWorker.clear();
          }

          promises.push(serviceWorker.clear());

          promises.push(
            navigator.serviceWorker.getRegistration().then((registration) => {
              if (registration) {
                return registration.unregister();
              }
            })
          );

          return Promise.all(promises);
        };

        serviceWorker.clear = () => {
          return $window.caches.keys().then((cacheNames) => {
            return Promise.all(
              cacheNames.map((cacheName) => {
                return $window.caches.delete(cacheName);
              })
            );
          });
        };

        serviceWorker.isSupported = () => supported;

        if (serviceWorker.isSupported()) {
          serviceWorker
            .isEnabled()
            .then((swEnabled) => {
              if (swEnabled) {
                return getRegistration(scope);
              }

              return null;
            })
            .then((registration) => {
              if (!registration) {
                return;
              }

              registration.onupdatefound = once(() => {
                $rootScope.$apply(() => {
                  emitNewVersion();
                });
              });

              if (typeof registration.update === 'function') {
                registration.update();
              }
            });
        }

        return serviceWorker;
      };
    },
  ]).name;
