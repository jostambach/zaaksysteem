// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import getFullName from './getFullName';

describe('The `getFullName` function', () => {
  test('returns `display_name` if `subject_type` is not `person`', () => {
    const name = getFullName({
      instance: {
        subject_type: 'company',
        display_name: 'Foo corp.',
      },
    });

    expect(name).toBe('Foo corp.');
  });

  describe('if the `subject_type` is `person`', () => {
    test('returns `first_names` and `surname` joined by a space if `first_names` is not empty', () => {
      const name = getFullName({
        instance: {
          subject_type: 'person',
          display_name: 'Fred Foobar the third',
          subject: {
            instance: {
              first_names: 'Fred Foster',
              surname: 'Foobar',
            },
          },
        },
      });

      expect(name).toBe('Fred Foster Foobar');
    });

    test('returns `surname` if `first_names` is empty', () => {
      const name = getFullName({
        instance: {
          subject_type: 'person',
          display_name: 'Fred Foobar',
          subject: {
            instance: {
              first_names: null,
              surname: 'Foobar',
            },
          },
        },
      });

      expect(name).toBe('Foobar');
    });
  });
});
