// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import assign from 'lodash/assign';

const createConfigurationReducer = (composedReducer, scope, configuration) =>
  composedReducer({ scope }, configuration).reduce((config) =>
    assign({}, configuration, {
      isCorrectlyConfigured: Boolean(
        config.appointmentInterfaceUuid &&
          config.productId &&
          config.locationId &&
          config.requestor
      ),
    })
  );

export default createConfigurationReducer;
