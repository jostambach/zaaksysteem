// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import keyBy from 'lodash/keyBy';
import identity from 'lodash/identity';
import mapValues from 'lodash/mapValues';
import isArray from 'lodash/isArray';
import compact from 'lodash/compact';
import { floatToCurrency, numberToString } from '../../../util/number';

export default (attribute, source) => {
  let value = source;

  switch (attribute.type) {
    case 'checkbox':
      if (!isArray(value)) {
        value = [value];
      }

      value = mapValues(keyBy(compact(value), identity), () => true);

      if (Object.keys(value).length === 0) {
        value = null;
      }
      break;
    case 'date':
      if (value) {
        value = new Date(value);
      }
      break;
    case 'numeric':
      if (typeof value === 'number') {
        value = numberToString(value);
      }
      break;
    case 'valuta':
    case 'valutaex':
    case 'valutaex21':
    case 'valutaex6':
    case 'valutain':
    case 'valutain21':
    case 'valutain6':
      if (typeof value === 'number') {
        value = floatToCurrency(value);
      }
      break;
  }

  return value;
};
