// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import fuzzy from 'fuzzy';
import flattenDeep from 'lodash/flattenDeep';
import get from 'lodash/get';
import isArray from 'lodash/isArray';
import take from 'lodash/take';
import uniq from 'lodash/uniq';
import seamlessImmutable from 'seamless-immutable';
import wordNgrams from 'word-ngrams';
import getAttributes from './getAttributes';
import getCaseValueFromAttribute from './getCaseValueFromAttribute';

const NGRAM_RANGE = [1, 2, 3];

// cf. https://github.com/Syeoryn/textAnalysisSuite/blob/master/Source/nGrams.js#L10
export const SENTENCE_SPLITTERS = [';', '.', '!', '?'];

export const NGRAM_BUILD_OPTIONS = {
  caseSensitive: false,
  includePunctuation: false,
};

let index = seamlessImmutable([]);
let results;
let matches;
let attributes;

// There must be at least on character that is not a sentence splitter
export const isSafeSubject = (value) =>
  new RegExp(`[^${SENTENCE_SPLITTERS.join()}]`, 'i').test(value);

const filterAttributeValue = (value) => value && isSafeSubject(value);

function getNGramsFromElement(el) {
  if (!attributes) {
    return null;
  }

  return attributes
    .map((attrName) => getCaseValueFromAttribute(attrName, el))
    .filter(filterAttributeValue)
    .map((elValue) => {
      let stringValue = isArray(elValue) ? elValue.join() : elValue;

      return NGRAM_RANGE.map((nGramLength) => {
        return wordNgrams.listAllNGrams(
          wordNgrams.buildNGrams(stringValue, nGramLength, NGRAM_BUILD_OPTIONS)
        );
      });
    });
}

export default {
  init(config) {
    attributes = getAttributes(config)
      .voorstel.filter((attr) => attr.external_name !== 'voorsteldocumenten')
      .map((attr) => get(attr, 'internal_name.searchable_object_id'));
  },
  getAttributesToIndex: () => attributes,
  addToIndex(elements) {
    index = seamlessImmutable(
      uniq(flattenDeep(elements.map(getNGramsFromElement)))
    );
  },
  getFromIndex(query) {
    results = take(fuzzy.filter(query, index.asMutable()), 15);
    matches = results.map((el) => el.string);

    return matches;
  },
};
