BEGIN;

ALTER TABLE interface ADD COLUMN date_deleted TIMESTAMP WITHOUT TIME ZONE;

COMMIT;