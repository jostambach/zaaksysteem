BEGIN;

ALTER TABLE subject ADD COLUMN username VARCHAR;
UPDATE subject SET username = md5(random()::text);
ALTER TABLE subject ALTER COLUMN username SET NOT NULL;

COMMIT;
