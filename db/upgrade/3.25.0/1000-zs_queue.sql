BEGIN;

    CREATE TABLE queue (
        id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
        object_id UUID REFERENCES object_data(uuid),
        status TEXT NOT NULL DEFAULT 'pending' CHECK ( status IN ('pending', 'running', 'finished', 'failed') ),
        type TEXT NOT NULL,
        label TEXT NOT NULL,
        data TEXT NOT NULL DEFAULT '{}',
        date_created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
        date_started TIMESTAMP WITHOUT TIME ZONE,
        date_finished TIMESTAMP WITHOUT TIME ZONE
    );

COMMIT;
