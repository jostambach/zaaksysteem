BEGIN;

    ALTER TABLE object_bibliotheek_entry ADD COLUMN name TEXT;
    UPDATE object_bibliotheek_entry SET name = od.properties::jsonb->'values'->'name'->>'value' FROM object_data od WHERE od.uuid = object_bibliotheek_entry.object_uuid;
    ALTER TABLE object_bibliotheek_entry ALTER COLUMN name SET NOT NULL;

    ALTER TABLE bibliotheek_categorie ADD COLUMN uuid UUID DEFAULT uuid_generate_v4() NOT NULL;
    ALTER TABLE bibliotheek_kenmerken ADD COLUMN uuid UUID DEFAULT uuid_generate_v4() NOT NULL;
    ALTER TABLE bibliotheek_sjablonen ADD COLUMN uuid UUID DEFAULT uuid_generate_v4() NOT NULL;
    -- Already exists
    --ALTER TABLE bibliotheek_notificaties ADD COLUMN uuid UUID DEFAULT uuid_generate_v4() NOT NULL;

    CREATE UNIQUE INDEX ON bibliotheek_categorie(uuid);
    CREATE UNIQUE INDEX ON bibliotheek_kenmerken(uuid);
    CREATE UNIQUE INDEX ON bibliotheek_sjablonen(uuid);
    -- Doesn't already exist:
    CREATE UNIQUE INDEX ON bibliotheek_notificaties(uuid);

COMMIT;
