BEGIN;

  CREATE TYPE public.preferred_contact_channel AS ENUM (
    'email',
    'mail',
    'pip',
    'phone'
  );

  ALTER TABLE natuurlijk_persoon ADD COLUMN preferred_contact_channel public.preferred_contact_channel default 'pip' NOT NULL;
  ALTER TABLE gm_natuurlijk_persoon ADD COLUMN preferred_contact_channel public.preferred_contact_channel default 'pip' NOT NULL;
  ALTER TABLE bedrijf ADD COLUMN preferred_contact_channel public.preferred_contact_channel default 'pip' NOT NULL;
  ALTER TABLE gm_bedrijf ADD COLUMN preferred_contact_channel public.preferred_contact_channel default 'pip' NOT NULL;

COMMIT;
