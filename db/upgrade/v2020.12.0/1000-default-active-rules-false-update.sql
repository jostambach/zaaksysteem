BEGIN;
  UPDATE zaaktype_regel SET active = false where settings::jsonb @> '{"active": null }' and active = true and settings not like '^%';
COMMIT;
