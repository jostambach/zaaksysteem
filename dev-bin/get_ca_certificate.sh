#! /bin/bash

docker-compose run --no-deps --rm frontend cat /etc/nginx/ssl/ca.crt \
    | tee dev.zaaksysteem.nl.pem
