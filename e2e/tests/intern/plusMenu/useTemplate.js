import {
    openPageAs
} from './../../../functions/common/navigate';
import {
    useTemplate
} from './../../../functions/intern/plusMenu';
import {
    openTab
} from './../../../functions/intern/caseView/caseNav';
import waitForElement from './../../../functions/common/waitForElement';

describe('when opening case 110', () => {
    beforeAll(() => {
        openPageAs('admin', 110);
    });

    describe('when using a template via the plusmenu to create an odt', () => {
        beforeAll(() => {
            const data = {};

            useTemplate(data);
            openTab('docs');
            waitForElement('.document-list-main');
        });

        it('there should be an odt in the documentstab', () => {
            expect($('.document-list-main').getText()).toContain('Plusknop zaak.odt');
        });

        afterAll(() => {
            openTab('phase');
        });
    });

    describe('when using a template via the plusmenu to create an pdf', () => {
        beforeAll(() => {
            const data = {
                filetype: 'pdf'
            };

            useTemplate(data);
            openTab('docs');
        });

        it('there should be a pdf in the documentstab', () => {
            expect($('.document-list-main').getText()).toContain('Plusknop zaak.pdf');
        });

        afterAll(() => {
            openTab('phase');
        });
    });

    describe('when using a template via the plusmenu to create a document as case document', () => {
        beforeAll(() => {
            const data = {
                template: 'Plusknop zaak als zaakdocument',
                caseDocument: 'Document'
            };

            useTemplate(data);
            openTab('docs');
        });

        it('there should be a document in the documentstab with the case document label', () => {
            expect($('.document-list-main').getText()).toContain('Plusknop zaak als zaakdocument.odt\nDocument');
        });

        afterAll(() => {
            openTab('phase');
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
