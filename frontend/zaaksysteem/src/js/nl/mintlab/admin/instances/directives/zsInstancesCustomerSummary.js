// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.admin.instances')
    .directive('zsInstancesCustomerSummary', [
      'zsApi',
      function (api) {
        var translations = {
          government: 'Overheid',
          commercial: 'Commercieel',
          lab: 'Lab',
          development: 'Development',
          staging: 'Staging',
          acceptance: 'Acceptatie',
          testing: 'Testing',
          preprod: 'Preproductie',
        };

        return {
          require: ['zsInstancesCustomerSummary'],
          restrict: 'E',
          replace: true,
          template:
            '<div class="summary-block">' +
            '<div class="summary-block-title">' +
            'Overzicht' +
            '<button class="summary-block-title-edit-button button button-smaller button-secondary right" data-zs-popup="\'/html/admin/instances/instances.html#customer-edit\'"data-ng-click="openPopup()" data-ng-show="editable()">' +
            '<i class="icon-font-awesome icon-pencil icon-only"></i>' +
            '</button>' +
            '</div>' +
            '<div class="summary-block-content">' +
            '<div class="summary-row form-field">' +
            '<div class="summary-row-label form-field_label">' +
            'Template' +
            '</div>' +
            '<div class="summary-row-content form-field-input">' +
            '<[customer().instance.template]>' +
            '</div>' +
            '</div>' +
            '<div class="summary-row form-field">' +
            '<div class="summary-row-label form-field_label">' +
            'Short name' +
            '</div>' +
            '<div class="summary-row-content form-field-input">' +
            '<[customer().instance.shortname]>' +
            '</div>' +
            '</div>' +
            '<div class="summary-row form-field">' +
            '<div class="summary-row-label form-field_label">' +
            'Type' +
            '</div>' +
            '<div class="summary-row-content form-field-input">' +
            '<[instancesCustomerSummary.getType()]>' +
            '</div>' +
            '</div>' +
            '<div class="summary-row form-field" data-ng-show="editable()">' +
            '<div class="summary-row-label form-field_label">' +
            'Read-only' +
            '</div>' +
            '<div class="summary-row-content form-field-input">' +
            '<[customer().instance.read_only? "Ja" : "Nee" ]>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>',
          scope: {
            customer: '&',
            editable: '&',
          },
          controller: [
            '$scope',
            function ($scope) {
              var ctrl = this;

              ctrl.link = angular.noop;

              ctrl.handleEditSubmit = function ($values) {
                return api
                  .post({
                    url:
                      '/api/v1/controlpanel/' +
                      $scope.customer().reference +
                      '/update',
                    data: $values,
                  })
                  .then(function (result) {
                    _.extend(
                      $scope.customer().instance,
                      result.data[0].instance
                    );
                  });
              };

              ctrl.getType = function () {
                var customerConfig = $scope.customer(),
                  template;

                if (customerConfig) {
                  template =
                    translations[customerConfig.instance.customer_type] ||
                    customerConfig.instance.customer_type;
                }

                return template;
              };
            },
          ],
          controllerAs: 'instancesCustomerSummary',
          link: function (scope, element, attrs, controllers) {
            controllers.shift().link(controllers);
          },
        };
      },
    ]);
})();
