// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case').directive('zsCaseWebformObjectField', [
    function () {
      return {
        require: ['zsCaseWebformObjectField', '^zsCaseWebformField'],
        controller: [
          '$scope',
          '$attrs',
          '$element',
          '$timeout',
          'sessionService',
          'composedReducer',
          function (
            $scope,
            $attrs,
            $element,
            $timeout,
            sessionService,
            composedReducer
          ) {
            var sessionResource = sessionService.createResource($scope);

            var sessionReducer = composedReducer(
              { scope: $scope },
              sessionResource
            ).reduce(function (session) {
              return session ? session.instance : {};
            });

            var ctrl = this,
              zsCaseWebformField;

            ctrl.limit = Number.MAX_VALUE;
            ctrl.objects = [];

            $scope.config = function () {
              return sessionReducer.data().configurable;
            };

            ctrl.setControls = function () {
              zsCaseWebformField = arguments[0];

              zsCaseWebformField.setGetter(function () {
                return ctrl.objects;
              });

              zsCaseWebformField.setSetter(function (value) {
                if (!value) {
                  value = [];
                }

                if (!_.isArray(value)) {
                  value = [value];
                } else {
                  value = angular.copy(value);
                }
                ctrl.objects = value;
              });
            };

            ctrl.addObject = function (obj) {
              var index = _.indexOf(ctrl.objects, obj);
              if (index === -1) {
                while (ctrl.objects.length >= ctrl.limit) {
                  ctrl.removeObject(ctrl.objects[0]);
                }
                ctrl.objects.push(obj);
                zsCaseWebformField.invalidate();
              }
            };

            ctrl.removeObject = function (obj) {
              var index = _.indexOf(ctrl.objects, obj);
              if (index !== -1) {
                ctrl.objects.splice(index, 1);
                zsCaseWebformField.invalidate();
              }

              $timeout(
                function () {
                  $element.find('input')[0].focus();
                },
                0,
                false
              );
            };

            ctrl.handleObjectSelect = function ($object) {
              var obj = {
                bag_id: $object.object.id,
                human_identifier: $object.label,
                address_data: $object.object,
              };

              ctrl.addObject(obj);
            };

            ctrl.setList = function (list) {
              ctrl.objects = angular.copy(list);
            };

            if ($attrs.zsCaseWebformObjectFieldLimit) {
              ctrl.limit = parseInt($attrs.zsCaseWebformObjectFieldLimit, 10);
            }

            return ctrl;
          },
        ],
        controllerAs: 'caseWebformObjectField',
        link: function (scope, element, attrs, controllers) {
          controllers[0].setControls.apply(
            controllers[0],
            controllers.slice(1)
          );
        },
      };
    },
  ]);
})();
