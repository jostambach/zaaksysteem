// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

angular
  .module('Zaaksysteem.docs')
  .controller('nl.mintlab.docs.FileCopyController', [
    '$scope',
    '$window',
    'smartHttp',
    'translationService',
    'snackbarService',
    'caseRelatedCases',
    function (
      $scope,
      $window,
      smartHttp,
      translationService,
      snackbarService,
      caseRelatedCases
    ) {
      function isFolder(item) {
        return item._entityType === 'folder';
      }

      function isFile(item) {
        return !isFolder(item);
      }

      function flattenFile(file) {
        return file.id;
      }

      function getSelectedParentFolder(item, selectedFolders) {
        return _.find(selectedFolders, function (folder) {
          return item._parent && folder.id === item._parent.id;
        });
      }

      function findSelectedParentFolder(item, selectedFolders) {
        var selectedParentFolder = getSelectedParentFolder(
          item,
          selectedFolders
        );

        if (!selectedParentFolder && item._parent) {
          return findSelectedParentFolder(item._parent, selectedFolders);
        }

        return selectedParentFolder;
      }

      function flattenFolder(folder) {
        return _.assign(
          { id: folder.id },
          folder.children.length
            ? { children: folder.children.map(flattenFolder) }
            : {},
          folder.files.length ? { files: folder.files.map(flattenFile) } : {}
        );
      }

      function emptyFolders(item) {
        if (isFolder(item)) {
          return _.assign({}, item, {
            children: [],
            files: [],
          });
        }

        return item;
      }

      function nestFolderStructure(selectedItems) {
        var selectedFolders = selectedItems.filter(isFolder);

        return selectedItems.reduce(function (acc, item) {
          var parentFolder = findSelectedParentFolder(item, selectedFolders);

          if (parentFolder) {
            if (isFile(item)) {
              parentFolder.files.push(item);
            } else {
              parentFolder.children.push(item);
            }

            return acc;
          }

          acc.push(item);
          return acc;
        }, []);
      }

      function rebuildTreeStructure() {
        var selectedItems = $scope.selectedFiles.map(emptyFolders);
        var nestedItems = nestFolderStructure(selectedItems);

        return nestedItems.reduce(
          function (acc, item) {
            isFolder(item)
              ? acc.children.push(flattenFolder(item))
              : acc.files.push(flattenFile(item));

            return acc;
          },
          {
            files: [],
            children: [],
          }
        );
      }

      $scope.copyToCase = null;
      $scope.isLoading = false;

      $scope.getType = function () {
        return 'case';
      };

      $scope.onKeyUp = function (query) {
        if (!query || query == '') {
          $scope.copyToCase = null;
        }
      };

      $scope.selectedSuggestion = function (suggestion) {
        if (!suggestion) {
          throw new Error('Received no suggestion-case.');
        }

        $scope.copyToCase = {
          name: suggestion.label,
          number: suggestion.data.id,
          id: suggestion.id,
        };
      };

      $scope.startCopy = function () {
        if (!$scope.copyToCase) {
          throw new Error('No case found to copy to.');
        }
        if (!$scope.selectedFiles || $scope.selectedFiles.length == 0) {
          throw new Error('No files found to copy.');
        }

        $scope.isLoading = true;

        function getDutchErrorMessage(type) {
          var errorType = type.split('/').pop();
          var errors = {
            current_case: 'De geselecteerde zaak is deze zaak.',
            not_found: 'De geselecteerde zaak kan niet worden gevonden.',
            closed: 'De geselecteerde zaak is afgehandeld.',
            unauthorized:
              'U heeft niet voldoende rechten om documenten aan deze zaak toe te voegen.',
            already_exists: 'Het bestand bestaat reeds in de zaak.',
            default: 'Onbekend. Neem contact op met de beheerder.',
          };

          return (
            '<br />Foutmelding: ' + (errors[errorType] || errors['default'])
          );
        }

        smartHttp
          .connect({
            url: '/api/case/' + $scope.caseId + '/file/copy_to_case',
            method: 'POST',
            data: _.assign(
              {
                case_id: $scope.copyToCase.number,
              },
              rebuildTreeStructure()
            ),
            blocking: false,
          })
          .success(function () {
            snackbarService.info(
              'De document(en) zijn succesvol gekopieerd naar ' +
                $scope.copyToCase.name +
                '.'
            );
          })
          .error(function (error) {
            var errorMessage =
              'Er is een fout opgetreden bij het kopi&euml;ren.';
            if (error && error.result && error.result.length > 0) {
              var errorType = error.result[0].type;
              errorMessage += getDutchErrorMessage(errorType);
            }
            snackbarService.error(errorMessage);
          })
          .finally(function () {
            $scope.copyToCase = null;
            $scope.isLoading = false;
            $scope.closePopup();
          });
      };

      $scope.$watch('copyType', function () {
        if ($scope.copyType !== 'related') return;

        $scope.isLoading = true;

        caseRelatedCases
          .getRelatedCases($scope.caseId)
          .then(function (relatedCases) {
            if (relatedCases.length > 0) {
              $scope.copyToCase = relatedCases[0];
            }
            $scope.relatedCases = relatedCases;
          })
          .catch(function (error) {
            if (error) {
              snackbarService.error(
                'Er is een fout opgetreden bij het ophalen van de gerelateerde zaken. Foutmelding: ' +
                  error.data.result.instance.message
              );
            }
            $scope.closePopup();
          })
          .finally(function () {
            $scope.isLoading = false;
          });
      });
    },
  ]);
