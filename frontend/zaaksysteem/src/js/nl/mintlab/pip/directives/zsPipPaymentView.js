// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.pip').directive('zsPipPaymentView', [
    function () {
      var numYears = 7;

      return {
        scope: true,
        controller: [
          function () {
            var ctrl = this,
              years = [];

            function calcYears() {
              var curYear = new Date().getFullYear(),
                i,
                l,
                years = [];

              for (i = 0, l = numYears; i < l; ++i) {
                years.push(curYear - i);
              }

              return years;
            }

            ctrl.getYears = function () {
              return years;
            };

            years = calcYears();

            return ctrl;
          },
        ],
        controllerAs: 'pipPaymentView',
      };
    },
  ]);
})();
