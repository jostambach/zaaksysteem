// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular.module('Zaaksysteem.directives').directive('zsContextmenu', [
    '$document',
    '$timeout',
    'templateCompiler',
    function ($document, $timeout, templateCompiler) {
      var getMousePosition = window.zsFetch(
          'nl.mintlab.utils.dom.getMousePosition'
        ),
        getViewportSize = window.zsFetch(
          'nl.mintlab.utils.dom.getViewportSize'
        ),
        cancelEvent = window.zsFetch('nl.mintlab.utils.events.cancelEvent'),
        body = $document.find('body'),
        overlay = angular.element($document[0].createElement('div'));

      overlay.addClass('context-menu-overlay');
      overlay.css('position', 'fixed');
      overlay.css('top', '0');
      overlay.css('left', '0');
      overlay.css('width', '100%');
      overlay.css('height', '100%');
      overlay.css('background', 'transparent');

      return {
        compile: function (/*tElement, tAttrs*/) {
          return function link(scope, element, attrs) {
            var tpl, compiler, menu, isOpen;

            function onContextMenu(event) {
              var pos;

              if (!compiler) {
                return;
              }

              pos = getMousePosition(event);

              cancelEvent(event);
              openMenu(pos.x, pos.y);
            }

            function onOverlayClick(/*event*/) {
              closeMenu();
            }

            function onMenuClick(/*event*/) {
              closeMenu();
            }

            function positionMenu(cx, cy) {
              var width = menu[0].clientWidth,
                height = menu[0].clientHeight,
                viewportSize = getViewportSize(),
                x,
                y;

              if (cx + width > viewportSize.width) {
                x = cx - width;
              } else {
                x = cx;
              }

              if (cy + height > viewportSize.height) {
                y = cy - height;
              } else {
                y = cy;
              }

              menu.css('top', y + 'px');
              menu.css('left', x + 'px');
            }

            function openMenu(x, y) {
              if (isOpen) {
                return;
              }

              isOpen = true;
              body.append(overlay);

              $timeout(function () {
                compiler(scope, function (clonedElement /*, scope*/) {
                  menu = clonedElement;
                  menu.css('opacity', '0');
                  menu.css('position', 'fixed');
                  menu.bind('click', onMenuClick);
                  overlay.after(menu);
                  $timeout(function () {
                    menu.css('opacity', '1');
                    positionMenu(x, y);
                  });
                });
              });

              overlay.bind('click', onOverlayClick);
              body.bind('contextmenu', onBodyContextMenu);
              $document.bind('keydown', onKeyDown);

              scope.$emit('contextmenuopen');
            }

            function closeMenu() {
              if (!isOpen) {
                return;
              }

              isOpen = false;

              if (menu) {
                menu.unbind('click', onMenuClick);
                menu.remove();
                menu = null;
              }

              overlay.remove();
              overlay.unbind('click', onOverlayClick);
              body.unbind('contextmenu', onBodyContextMenu);
              $document.unbind('keydown', onKeyDown);

              scope.$emit('contextmenuclose');
            }

            function onKeyDown(event) {
              if (event.keyCode === 27) {
                closeMenu();
              }
            }

            function onBodyContextMenu(event) {
              cancelEvent(event);
            }

            attrs.$observe('zsContextmenu', function () {
              tpl = scope.$eval(attrs.zsContextmenu);
              if (tpl) {
                templateCompiler.getCompiler(tpl).then(function (cpl) {
                  compiler = cpl;
                });
              } else {
                compiler = null;
              }
            });

            element.bind('contextmenu', onContextMenu);
          };
        },
      };
    },
  ]);
})();
