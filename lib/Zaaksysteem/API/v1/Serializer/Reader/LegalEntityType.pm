package Zaaksysteem::API::v1::Serializer::Reader::LegalEntityType;
use Moose;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::LegalEntityType - Read LegalEntityType objects.

=head1 SYNOPSIS

    my $reader = Zaaksysteem::API::v1::Serializer::Reader::LegalEntityType->grok($object);

    my $data = $reader->($serializer, $object);

=head1 DESCRIPTION

This class implements a serializer reader for
L<Zaaksysteem::Backend::Object::Data::Component> objects.

This class should not be used outside of the
L<Zaaksysteem::API::v1::Serializer> infrastructure.

=head1 METHODS

=head2 grok

Implements sub required by L<Zaaksysteem::API::v1::Serializer>.

=cut

sub grok {
    my ($class, $object) = @_;

    return unless blessed $object && $object->isa($class->class);

    my $reader_method = 'read_legal_entity_type';

    if ($class->can($reader_method)) {
        return sub { $class->$reader_method(@_) };
    }

    return;
}

=head2 class

Implements sub required by L<Zaaksysteem::API::v1::Serializer::ReaderRole>.

=cut

sub class { 'Zaaksysteem::Object::Types::LegalEntityType' }

=head2 read_controlpanel

Reader for controlpanel object

=cut

sig read_legal_entity_type => 'Zaaksysteem::API::v1::Serializer, Object => HashRef';

sub read_legal_entity_type {
    my ($self, $serializer, $object) = @_;

    return {
        type => 'legal_entity_type',
        reference => undef,
        instance => {
            $self->_get_object_attributes($serializer, $object, qw(code label description active)),
        }
    };
}

sub _get_object_attributes {
    my ($self, $serializer, $object, @attribute_list) = @_;
    return map { $_ =>  $object->$_ } @attribute_list;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
