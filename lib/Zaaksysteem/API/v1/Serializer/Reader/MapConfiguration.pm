package Zaaksysteem::API::v1::Serializer::Reader::MapConfiguration;

use Moose;

with 'Zaaksysteem::API::v1::Serializer::DispatchReaderRole';

=head1 NAME

Zaaksysteem::API::v1::Serializer::Reader::MapConfiguration - MapConfiguration readers, for the Map Module

=head1 DESCRIPTION

This serializer encodes various map-related objects into a valid APIv1 JSON structure.

=head1 OBJECT TYPES

The following object types will be converted via this reader:

=head1 METHODS

=head2 dispatch_map

The dispatcher for various Map Objects

=head2 ol_settings

OpenLayer settings

=head3 ol_layer_wms

OpenLayers Layer WMS

=cut

sub dispatch_map {
    return (
        'Zaaksysteem::Object::Types::MapConfiguration' => sub { __PACKAGE__->read_map_configuration(@_) },
    );
}

=head2 read_map_configuration

Return the contents of a OpenLayer WMS layer

=cut

sub read_map_configuration {
    my ($class, $serializer, $object) = @_;

    return {
        type => 'ol_settings',
        reference => undef,
        instance => {
            map_application => $object->map_application,
            map_application_url => $object->map_application_url,
            map_center => $object->map_center,
            wms_layers => [ map({ $serializer->read($_) } @{ $object->wms_layers }) ],
            xml_namespaces => [ map({ $serializer->read($_) } @{ $object->xml_namespaces }) ],
        }
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
