package Zaaksysteem::Backend::Rules::Rule::Condition::PreferredContactchannel;

use Moose::Role;
use BTTW::Tools;

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Condition::PreferredContactchannel - Handles prefered contact channel

=head1 SYNOPSIS

=head1 DESCRIPTION

=cut

my %map = (
 'Persoonlijke internetpagina' => 'pip',
 'E-mail' => 'email',
 'Post' => 'mail',
 'Telefoon' => 'phone',
);

after 'BUILD' => sub {
    my $self        = shift;

    if ($self->attribute eq 'preferred_contact_channel') {
        my $val = $self->values;
        $self->values([map { $map{$_} } @$val]);
        $self->attribute('case.requestor.preferred_contact_channel')
    }
};

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
