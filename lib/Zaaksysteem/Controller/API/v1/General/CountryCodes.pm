package Zaaksysteem::Controller::API::v1::General::CountryCodes;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::General::CountryCodes - API v1 controller for country codes

=head1 DESCRIPTION

This controller returns the 'landcodes' in use by Dutch local governments and ISO 3166

=cut

use BTTW::Tools;
use DateTime;
use Zaaksysteem::API::v1::ArraySet;
use Zaaksysteem::Constants qw(RGBZ_LANDCODES);
use Zaaksysteem::Object::Types::CountryCode;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('extern', 'intern', 'allow_pip');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/general/country_codes> routing namespace.

=cut

sub base : Chained('/api/v1/general/base') : PathPart('country_codes') : CaptureArgs(0) {
    my ($self, $c)      = @_;

    $self->get_all_country_codes($c);
}

=head2 instance_base

Reserves the C</api/v1/general/country_codes/[COUNTRY_CODE_UUID]> routing
namespace.

=cut

sub instance_base : Chained('base') : PathPart('') : CaptureArgs(1) {
    my ($self, $c, $uuid) = @_;

    my $entities = RGBZ_LANDCODES();

    if ($entities->{$uuid}) {
        $c->stash->{$self->namespace} = Zaaksysteem::Object::Types::CountryCode->new(
            dutch_code  => $uuid,
            label       => $entities->{$uuid},
        );
    }
    else {
        throw('api/v1/general/country_codes/not_found', sprintf(
            'Unable to find country code "%s"',
            $uuid,
        ));
    }
}

=head2 list

=head3 URL Path

C</api/v1/general/country_codes>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;
    $self->list_set($c);
}

=head2 get

=head3 URL Path

C</api/v1/general/country_codes/[COUNTRY_CODE_UUID]>

=cut

sub get : Chained('instance_base') : PathPart('') : Args(0) : RO {
    my ($self, $c) = @_;
    $self->get_object($c);
}

=head1 METHODS

=head2 get_all_country_codes

Get all the legal entitities from Zaaksysteem

=cut

sub get_all_country_codes {
    my ($self, $c)      = @_;

    my $entities = RGBZ_LANDCODES();

    my @o;
    foreach (sort keys %$entities) {
        push(@o, Zaaksysteem::Object::Types::CountryCode->new(
            dutch_code  => $_,
            label       => $entities->{$_},
        ));
    }

    @o = sort { $a->label cmp $b->label } @o;

    my $set = Zaaksysteem::API::v1::ArraySet->new(
        content       => \@o,
        allow_rows_per_page => 500,
    );

    $c->stash->{set} = $set;
}

has '+namespace' => (
    default => 'country_codes'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
