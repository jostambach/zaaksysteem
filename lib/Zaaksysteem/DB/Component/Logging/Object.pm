package Zaaksysteem::DB::Component::Logging::Object;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head1 NAME

Zaaksysteem::DB::Component::Logging::Object - 'Base' role for non-case object interaction events

=head1 METHODS

=head2 event_category

This method overrides L<Zaaksysteem::DB::Component::Logging/event_category>
and hardcodes the return value to C<object-mutation>.

=cut

sub event_category { 'object-modification' }

=head2 object_description

A short description of the object type, used to display log entries.

=cut

sub object_description {
    my $self = shift;

    my $rv = $self->data->{object_type};

    if ($rv eq 'scheduled_job') {
        $rv = "Geplande zaak met zaaktype"
    }

    return $rv;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
