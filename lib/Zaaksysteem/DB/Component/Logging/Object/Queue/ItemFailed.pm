package Zaaksysteem::DB::Component::Logging::Object::Queue::ItemFailed;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head1 NAME

Zaaksysteem::DB::Component::Logging::Object::Queue:ItemFailed - Event subject
for queue item failure events.

=head1 METHODS

=head2 onderwerp

Overrides L<Zaaksysteem::Schema::Logging/onderwerp> to provide contextualized
human-readable subject lines for this event type.

=cut

sub onderwerp {
    my $self = shift;

    my $item = $self->data->{ item };

    if ($item->{ type } eq 'run_ordered_item_set') {
        # Skip over undefined items, only finished items get pruned.
        my @item_labels =  map { $_->label }
                          grep { defined }
                           map { $self->get_item($_) }
                               @{ $item->{ data }{ item_ids } };

        return sprintf(
            'Het uitvoeren van een actie tijdens faseovergang is mislukt, waardoor de volgende acties niet zijn uitgevoerd: %s.',
            join(', ', map { sprintf('"%s"', $_) } @item_labels )
        );
    }

    return sprintf('Faseactie "%s" is mislukt.', $self->data->{ item }{ label });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
