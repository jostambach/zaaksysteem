package Zaaksysteem::DB::Statistics;
use strict;
use warnings;

use feature 'state';

use base 'DBIx::Class::Storage::Statistics';

use Time::HiRes qw(time);
use BTTW::Tools;

my $start;
state $list = [];

sub reset_query_counters {
    $list = [];
}

sub query_list {
    return $list;
}

sub query_start {
    my $self = shift();
    my $sql = shift();
    my @params = @_;

    $start = time();
}

sub query_end {
    my $self = shift();
    my $sql = shift();
    my @params = @_;

    my $elapsed = sprintf("%0.6f", (time() - $start) * 1000 );

    push(@{$list}, { sql => $sql, took => $elapsed});

    $start = undef;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
