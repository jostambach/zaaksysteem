package Zaaksysteem::DB::Storage;
use warnings;
use strict;

use Zaaksysteem::DB::Statistics;
use BTTW::Tools;
use Log::Log4perl;

use parent qw(
    DBIx::Class::Storage::DBI::Replicated
    DBIx::Class::Storage::DBI::Pg
);


=head1 NAME

Zaaksysteem::DB::Storage - Statistics logging on top of DBIx::Class::Storage::DBI::Pg

=head1 ATTRIBUTES

=head2 _query_start_time

The time the measure was started

=head2 query_list

A store with a list of queries, for later reference

=cut

=head1 METHODS

=head2 log

Interface to log4perl. This is no Moose class, so manually added

=cut

my $logger;
sub log {
    my $self    = shift;
    my $cat     = shift;

    if ($cat && $cat =~ m/^(\.|::)/) {
        return Log::Log4perl->get_logger(ref($self) . $cat);
    } elsif($cat)  {
        return Log::Log4perl->get_logger($cat);
    } else {
        return $logger if $logger;
        return ($logger = Log::Log4perl->get_logger(ref($self)));
    }
}

=head2 reset_query_counters

Resets the query list

=cut

sub reset_query_counters {
    my $self = shift;

    $self->debugobj->reset_query_counters();
    return;
}

sub query_list {
    my $self    = shift;

    $self->debugobj->query_list();
}

sub new {
    my $self = shift->next::method(@_);

    my $stats = Zaaksysteem::DB::Statistics->new();

    $self->debug(1);

    foreach my $source ($self->all_storages) {
        $source->debugobj($stats);
    }

    return $self;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
