package Zaaksysteem::Email::Planned;
use Moose;

use BTTW::Tools;

=head1 NAME

Zaaksysteem:Email::Planned - Create planned e-mails via an API.

=head1 DESCRIPTION

=head1 SYNOPSIS

=cut

has case => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Model::DB::Zaak',
    required => 1,
);

has interface => (
    is      => 'ro',
    isa     => 'Zaaksysteem::Model::DB::Interface',
    lazy    => 1,
    default => sub {
        my $self      = shift;
        my $interface = $self->case->get_email_interface;
        return $interface if $interface;
        throw('zaak/scheduled_email/interface',
            "Interface 'geplande e-mail' is niet actief of geconfigureerd");

    },
);

=head2 get_pending_emails

Get all the pending e-mails for a case

=cut

sub get_pending_emails {
    my $self = shift;

    $self->interface;

    my $transactions = $self->case->pending_notifications;

    my @list;
    foreach my $transaction (@$transactions) {
        my $params = $transaction->get_processor_params;
        push(
            @list,
            {
                type      => 'transaction',
                reference => $transaction->uuid,
                instance  => {
                    reference => $transaction->uuid,
                    id        => $transaction->id,
                    date      => $transaction->date_next_retry->set_time_zone(
                        'Europe/Amsterdam'),
                    case => {
                        reference => $self->case->get_column('uuid'),
                        type      => 'case'
                    },
                    label         => $params->{label},
                    recipient     => $params->{notification}{recipient_type},
                    email_address => $params->{notification}{email},
                    notification_id => $params->{notification}{id},
                }
            }
        );
    }
    return \@list;
}

=head2 create

Create a planned e-mail. Primary used in the testsuite.

=cut

define_profile create => (
    required => {
        email          => 'Str',
        recipient_type => 'Str',
        behandelaar    => 'Str',
        bibliotheek_notificatie =>
            'Zaaksysteem::Model::DB::BibliotheekNotificaties',
    },
    optional => {
        scheduled_for        => 'DateTime',
        zaaktype_notificatie => 'Zaaksysteem::Schema::ZaaktypeNotificatie',
    },
);

sub create {
    my $self   = shift;
    my $params = assert_profile({@_})->valid;

    my $bn = delete $params->{bibliotheek_notificatie};
    $params->{id} = $bn->id;

    my $sf  = delete $params->{scheduled_for};
    my $ztn = delete $params->{zaaktype_notificatie};

    return $self->interface->process_trigger(
        'schedule_mail',
        {
            notification            => $params,
            case_id                 => $self->case->id,
            scheduled_for           => $sf,
            zaaktype_notificatie_id => $ztn ? $ztn->id : undef,
        }
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
