package Zaaksysteem::Export::CSV;
use Moose;

use BTTW::Tools;
use List::Util qw(any);

with qw(MooseX::Log::Log4perl);

=head1 NAME

Zaaksysteem::Export::CSV - Export Zaaksysteem to CSV

=head1 DESCRIPTION

This model hides the logic for developers to transform data to CSV.

=head1 SYNOPSIS

    use Zaaksysteem::Export::CSV;

    my $model = Zaaksysteem::Export::CSV->new(
        user => $subject
    );

=cut

has user => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema::Subject',
    required => 1,
);

has blacklist_mapping => (
    is      => 'ro',
    isa     => 'HashRef',
    default => sub { {} },
    lazy    => 1,
);

has blacklist => (
    is       => 'ro',
    isa      => 'ArrayRef',
    builder  => '_build_blacklist',
    lazy     => 1,
    init_arg => undef,
);

has objects => (
    is       => 'ro',
    isa      => 'Defined',
    required => 1
);

has lines => (
    is      => 'ro',
    isa     => 'ArrayRef',
    traits  => ['Array'],
    lazy    => 1,
    default => sub { [] },
    handles => {
        add_csv_line  => 'push',
        has_csv_lines => 'count',
    },
);

has direct => (
    is     => 'ro',
    isa    => 'Bool',
    writer => '_set_direct',
);

has no_header => (
    is      => 'ro',
    isa     => 'Bool',
    default => 0,
);

has _empty_header_data => (
    is => 'rw',
    isa => 'Bool',
    default => 1,
    init_arg => undef,
);

has column_order => (
    is        => 'rw',
    isa       => 'ArrayRef',
    default   => sub { [] },
    lazy      => 1,
    predicate => 'has_column_order',
);

has column_types => (
    is        => 'rw',
    isa       => 'ArrayRef',
    default   => sub { [] },
    lazy      => 1,
    predicate => 'has_column_types',
);

has attr_order => (
    is        => 'rw',
    isa       => 'ArrayRef',
    default   => sub { [] },
    lazy      => 1,
    predicate => 'has_attr_order',
);

has format => (
    is => 'ro',
    isa => 'Str',
    default => 'csv',
);

has converter => (
    isa     => 'Zaaksysteem::DocumentConverter',
    is      => 'ro',
    lazy    => 1,
    default => sub { return Zaaksysteem::DocumentConverter->new(); },
);

use constant EXPORT_FILETYPE_CONFIG => {
    xls  => { mimetype => 'application/vnd.ms-excel', extension => 'xls' },
    calc => {
        mimetype  => 'application/vnd.oasis.opendocument.spreadsheet',
        extension => 'ods'
    },
};

sub has_column_options {
    my $self = shift;
    foreach (qw(has_column_order has_column_types has_attr_order)) {
        return 1 if $self->$_;
    }
    return 0;
}

sub header_data {
    my $self = shift;
    return {
        $self->has_column_order ? (column_order => $self->column_order) : (),
        $self->has_column_types ? (column_types => $self->column_types) : (),
        $self->has_attr_order   ? (attr_order   => $self->attr_order)   : (),
    },
}

sub _build_blacklist {
    my $self = shift;

    return [] if $self->user->has_legacy_permission('view_sensitive_data');
    return [map { $_ } keys %{ $self->blacklist_mapping }];
}

sub _check_and_set_empty_header {
    my $self = shift;
    return unless $self->_empty_header_data;

    my $header_empty = any { is_empty_attr_label($_) } @{$self->column_order};
    $self->_empty_header_data($header_empty);
    return;
}

has _csv => (
    is      => 'ro',
    isa     => 'Text::CSV_XS',
    builder => '_text_csv',
    lazy    => 1,
);

has _fh => (
    is   => 'ro',
    isa  => 'File::Temp',
    lazy => 1,
    default => sub {
        my $fh = File::Temp->new(SUFFIX => '.csv');
        binmode($fh, ":utf8");
        return $fh;
    }
);

around add_csv_line => sub {
    my $orig = shift;
    my $self = shift;
    my @data = @_;

    if ($self->direct) {
        $self->_csv->say($self->_fh, $_ ) for @data;
        return;
    }
    $self->$orig(@_);
};

sub process {
    my $self   = shift;

    if ($self->no_header) {
        $self->_set_direct(1);
    }
    else {
        # Generate a header. Be careful, the first object might not have all the
        # entries for a propper header so we need to fix that glitch elsewhere
        $self->_load_csv_header();

        $self->_check_and_set_empty_header();

        if (!$self->_empty_header_data) {
            $self->_set_direct(1);
        }

        $self->add_csv_line($self->column_order);
    }

    my $result = $self->objects;

    if (blessed($result)
        && (   $result->isa('DBIx::Class::ResultSet')
            || $result->isa('Zaaksysteem::Object::Iterator')
        )
    ) {
        $result->reset;

        while (defined(my $entry = $result->next)) {
            $self->_parse_csv_from_entry($entry);
        }
    } else {
        foreach my $entry (@$result) {
            $self->_parse_csv_from_entry($entry);
        }
    }
    return 1;
}

sub is_empty_attr_label {
    my $val = shift;
    return 1 if (!defined $val || !length($val));
    return 0;
}

sub _parse_csv_from_entry {
    my ($self, $entry ) = @_;
    if (blessed($entry) && $entry->isa('DBIx::Class')) {
        if ($entry->can('csv_data')) {
            # It seems cases are now exported by csv_data
            if ($entry->can('blacklisted_columns')) {
                $entry->blacklisted_columns($self->blacklist);
            }

            # This is gonna be a performance hog..
            if ($self->_empty_header_data) {

                my $column_order = $self->column_order;
                my $header_new = $entry->csv_header;

                for (my $i = 0; $i < @$column_order; $i++) {
                    next unless is_empty_attr_label($column_order->[$i]);
                    my $label = $header_new->[$i]{label};
                    next if is_empty_attr_label($label);
                    $column_order->[$i] = $label;
                    $self->_check_and_set_empty_header;
                }
            }
            $self->add_csv_line($entry->csv_data);
        } else {
            my $rowdata    = { $entry->get_columns };
            my $row    = [
                map { $rowdata->{ $_ } } @{$self->column_order}
            ];
            $self->add_csv_line($row);
        }
    }
    elsif(blessed($entry) && $entry->isa('Zaaksysteem::Object')) {
        my @row;

        for my $attr_name (@{$self->attr_order}) {
            my $attr = $entry->attribute_instance($attr_name);

            my $value = $attr ? $attr->value : undef;

            if (ref $value eq 'ARRAY') {
                $value = join(',', @{ $value } );
            }

            push @row, $value;
        }

        $self->add_csv_line(\@row);
    }
    elsif (ref $entry eq 'HASH') {
        my $row = [
            map { $entry->{$_} } @{$self->column_order}
        ];
        $self->add_csv_line($row);
    }
    elsif (ref $entry eq 'ARRAY') {
        # An array is for who ever defined the data
        $self->add_csv_line($entry);
    }
    else {
        die("Unable to parse entry for CSV");
    }
}



sub _load_csv_header {
    my $self        = shift;

    return if $self->has_column_options;

    my $result = $self->objects;

    my $first_row;
    if (blessed($result) && $result->can('first')) {
        $first_row = $result->first;
    }
    elsif (ref $result eq 'ARRAY') {
        $first_row = $result->[0];
    }
    else {
        $self->log->debug(
            "No CSV header, object isn't an array or cannot do first");
        return;
    }

    unless (blessed($first_row)) {
        $self->log->debug("No CSV header, object isn't blessed");
        return;
    }

    if ($first_row->isa('Zaaksysteem::Object')) {
        $self->column_order($first_row->attribute_names);
        return;
    }
    if ($first_row->isa('DBIx::Class')) {
        if (!$first_row->can('csv_header')) {
            my %columns = $first_row->get_columns;
            $self->column_order([keys %columns]);
            return;
        }
        my $header = $first_row->csv_header;

        if (ref $header eq 'ARRAY' && ref $header->[0] eq 'HASH') {
            $self->_format_csv_header_from_dbix_class($header);
            return;
        }
        $self->column_order($header);
        return;
    }
}

sub _format_csv_header_from_dbix_class {
    my $self   = shift;
    my $header = shift;

    my @dates = qw(date timestamp timestamp_or_text);

    my ($type, @column_format, @label, @name);

    foreach (@$header) {
        $type = $_->{attribute_type};
        if ($type && any { $type eq $_ } @dates)
        {
            push @column_format, 'datetime';
        }
        else {
            push @column_format, 'default';
        }
        push(@label, $_->{label});
        push(@name,  $_->{attribute_name});
    }
    $self->column_types(\@column_format);
    $self->column_order(\@label);
    $self->attr_order(\@name);
    return;
}

sub _text_csv {
    return Text::CSV_XS->new(
        {
            'quote_char'          => '"',
            'escape_char'         => '"',
            'sep_char'            => ',',
            'eol'                 => "\n",
            'binary'              => 1,
            'allow_loose_quotes'  => 1,
            'allow_loose_escapes' => 1,
            'allow_whitespace'    => 1,
            'always_quote'        => 1
        }
    );
}

sub to_filehandle {
    my $self = shift;

    if ($self->direct) {
        $self->_fh->seek(0,0);
        return $self->_fh if $self->format eq 'csv';
        return $self->_to_document_converter($self->_fh);
    }

    my $fh = File::Temp->new();

    binmode($fh, ":utf8");

    my $txt_csv = _text_csv();

    $txt_csv->say($fh, $_) for @{$self->lines};

    $fh->seek(0,0);
    return $fh if $self->format eq 'csv';
    return $self->_to_document_converter($fh);
}

sub _to_document_converter {
    my ($self, $fh) = @_;

    $fh->seek(0,0);

    return $self->converter->convert_to_fh(
        destination_type => EXPORT_FILETYPE_CONFIG->{$self->format}{mimetype},
        source_filename  => $fh->filename,
        filter_options   => {
            column_types    => $self->column_types,
            force_from_type => 'text/csv',
            locale          => 'nl_NL',
        }
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
