package Zaaksysteem::General::Actions::API::v2;
use Moose;
use namespace::autoclean;

use BTTW::Tools;
use Zaaksysteem::Types qw(UUID);

=head1 NAME

Zaaksysteem::General::Actions::API::v2 - API v2 action calls

=head1 SYNOPSIS

=head1 METHODS

=head2 v2_redirect_catalog

Redirect to the catalog via a specific action for /api/v2 infra

=cut

sub v2_redirect_catalog {
    my ($c, $id, @uri_args) = @_;

    my $return_url = $c->session->{api}{v2}{casetype}{url};
    if (!$return_url) {
        $c->res->redirect($c->uri_for('/beheer/bibliotheek', @uri_args));
        return;
    }

    $id //= $c->session->{api}{v2}{casetype}{category};

    my $category_uuid;
    if ($id) {
        my $category = $c->model('DB::BibliotheekCategorie')->find($id);
        $category_uuid = $category->uuid if $category;
    }
    $category_uuid //= $c->session->{api}{v2}{casetype}{folder};

    $return_url =~ s/\{\}/$category_uuid/g;

    $c->log->debug(
        sprintf(
            "Found casetype_edit_return_url = %s; final return_url = %s",
            $c->session->{api}{v2}{casetype}{url}, $return_url
        )
    );

    delete $c->session->{api}{v2}{casetype};
    $c->res->redirect($return_url);
    return;
}


1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::General>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
