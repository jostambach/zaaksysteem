package Zaaksysteem::JSON;

use JSON;
use Moose;
use DateTime::Format::Pg;
use Scalar::Util 'blessed';

use constant SERIALIZER_MAP => {
    DateTime    => {
        serialize   => sub {
            return DateTime::Format::Pg->format_datetime(shift);
        },
        deserialize => sub {
            return DateTime::Format::Pg->parse_datetime(shift);
        }
    }
};

sub BUILD {
    warn "Use of Zaaksysteem::JSON is deprecated in favor of plain JSON(::XS) (de)serialization.";
}

=head1 METHODS

=head2 _encode_to_json

Return value: $json_string

The preferred Zaaksysteem function for converting a perl hash to a json binary
string

=cut

sub _encode_to_json {
    my $self        = shift;
    my $data        = shift;

    my $jsondata    = $self->serializer_for_json(
        $data
    );

    return encode_json ($data);
}

sub _deserialize_key {
    my $self        = shift;
    my $rawvalue    = shift;
    my $deserialize = shift;

    return unless defined($rawvalue);

    my $SERIALIZE_SUB_MAP   = SERIALIZER_MAP;

    if ($rawvalue =~ /^SERIALIZE::/) {
        my ($callback_name, $value) = $rawvalue
            =~ /^SERIALIZE::(.*?)::(.*)/;

        if (
            defined(
                $SERIALIZE_SUB_MAP->{
                    $callback_name
                }
            )
        ) {
            my $callback    = $SERIALIZE_SUB_MAP
                ->{ $callback_name }
                ->{ deserialize };

            $rawvalue = $callback->($value);
        }
    }

    return $rawvalue;

}

sub _serialize_key {
    my $self        = shift;
    my $rawvalue    = shift;
    my $deserialize = shift;

    return unless defined($rawvalue);

    my $SERIALIZE_SUB_MAP   = SERIALIZER_MAP;

    # No hash or array, maybe a code reference?
    for my $callback_name (keys %{ $SERIALIZE_SUB_MAP }) {
        my $callback    = $SERIALIZE_SUB_MAP
            ->{ $callback_name }
            ->{ serialize };

        if (UNIVERSAL::isa($rawvalue, $callback_name)) {
            $rawvalue = 'SERIALIZE::'
                . $callback_name . '::'
                . $callback->($rawvalue);
        }
    }

    return $rawvalue;
}

sub serializer_for_json {
    my $self        = shift;
    my ($data, $deserialize) = @_;

    my $SERIALIZE_SUB_MAP   = SERIALIZER_MAP;

    if (UNIVERSAL::isa($data, 'HASH')) {
        for my $key (keys %{ $data }) {
            next if !defined($data->{ $key });

            if (!ref($data->{ $key })) {
                if ($deserialize) {
                    $data->{ $key } = $self->_deserialize_key($data->{ $key }, $deserialize);
                }
                next;
            }

            if (
                !blessed($data->{$key}) &&
                (
                    UNIVERSAL::isa($data->{ $key }, 'HASH') ||
                    UNIVERSAL::isa($data->{ $key }, 'ARRAY')
                )
            ) {
                $self->serializer_for_json(
                    $data->{ $key }, $deserialize
                );

                next;
            }

            $data->{ $key } = $self->_serialize_key($data->{ $key }, $deserialize);
        }
    } elsif (UNIVERSAL::isa($data, 'ARRAY')) {
        for (my $i = 0; $i < scalar(@{ $data }); $i++) {
            next if !defined($data->[$i]);

            if (!ref($data->[$i])) {
                if ($deserialize) {
                    $data->[$i] = $self->_deserialize_key($data->[$i], $deserialize);
                }
                next;
            }

            if (
                !blessed($data->[$i]) &&
                (
                    UNIVERSAL::isa($data->[$i], 'HASH') ||
                    UNIVERSAL::isa($data->[$i], 'ARRAY')
                )
            ) {
                $self->serializer_for_json(
                    $data->[$i], $deserialize
                );

                next;
            }

            $data->[$i] = $self->_serialize_key($data->[$i], $deserialize);
        }
    }

    return $data;
}

=head2 _decode_from_json

Return value: $json_string

The preferred Zaaksysteem function for converting a json string to an internal
perl hash.

=cut

sub _decode_from_json {
    my $self        = shift;
    my $json_data   = shift;

    my $data = decode_json($json_data);

    return $self->serializer_for_json($data, 1);
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 BUILD

TODO: Fix the POD

=cut

=head2 serializer_for_json

TODO: Fix the POD

=cut

