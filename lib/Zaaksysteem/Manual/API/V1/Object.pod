=head1 NAME

Zaaksysteem::Manual::API::V1::Object - Object type documentation

=head1 DESCRIPTION

This page documents the generic notion of an object within the context of the
Zaaksysteem API v1 implementation.

=head1 INTRODUCTION

Interacting with the API means dealing with the v1 object model. At the end of
this page, you will have learned how the objects are structured, and what
information can be derived by skimming over any object's serialization.

At the highest level of abstraction, the object model is yet another OOP
implementation, most object-oriented developers should be right at home here.

We'll look at the L</"JSON serialization">, definition of object types, and
common uses in within the API.

=head1 JSON serialization

Since all v1 APIs used by Zaaksysteem enforce the use of JSON as serialization
format, it's useful to have a grasp of the generic layout of these
serializations and what certain values imply. Let's take a look at a fictional
'contract' object, and inspect the envelope and instance attributes.

=begin javascript

{
    "type": "contract",
    "reference": "3490e375-4c39-4520-87c2-30c2fd6addef",
    "instance": {
        "name": "Service contract XYZ",
        "start": "2015-05-07",
        "duration": "P2Y",
        "monthly_fee": 29.95,

        "service": {
            "type": "service",
            "reference": "d4ffefec-fb37-4d30-b500-c46917d74a92"
        },

        "contact": {
            "type": "contact",
            "instance": {
                "name": "Bernice Ratt",
                "email_address": "brat@example.com"
            }
        }
    }
}

=end javascript

=head2 Object envelope

All objects come wrapped in a JSON "envelope" object like the one above. The
envelope's C<type> must always be present, the C<reference> and C<instance>
fields are exclusively optional (at least one must be present).

=over 4

=item type

Every object exposed and used by Zaaksysteem has a type. Zaaksysteem uses this
information to lookup the type definition and validation constraints of object
instances.

Types may be namespaced using a C</> (e.g. C<type/subtype/subsubtype>).

=item reference

The C<reference> field must be either undefined (C<null>) or a canonical,
stringified L<UUID v4|https://tools.ietf.org/html/rfc4122> identifier.

If the value is undefined, it is implied that the object is volatile, and has
not been persisted in the Zaaksysteem object storage layer.

=item instance

The C<instance> field contains a collection of key-value pairs which map to
attribute names and associated values for the given object instance.

If the C<instance> key is omitted, or its value left undefined, the object
becomes an instance of a reference type, pointing to an existing object in the
object persistance layer. These reference instances can often still be used in
place of a full object instance, which is especially useful when relating
objects without having to fully serialize or process all existing object data
of the referent (object being referenced).

=back

=head2 Object instance data

The main point of interest will usually be the contents of the C<instance>
field in the envelope, which contains the specific data for the object.

From the example above, we can see there are at least 5 attributes defined
for the 'contract' type:

=over 4

=item name

This fields associates a plain string value with the C<name> attribute,
WYSIWYG.

=item start

While serialized as a string, this attribute has been declared to contain
C<date> values only. Zaaksysteem will automatically convert the string to a
date instance when interacting with the object.

=item duration

Similar to the C<start> field, but the value represents a period of time.

=item monthly_fee

This field 'obviously' implies values to be numbers, but Zaaksysteem will
also accept stringified numbers.

=item service

The value of this field is a reference to a 'service' object, which
presumably has additional information describing the service.

=item contact

This value is a volatile object with contact information about the
contractee. When submitting the 'contract' object for creation/updating to
Zaaksysteem, the contact object will be automatically created and properly
linked.

=back

For a complete overview of value types supported by the API, see
L<Zaaksysteem::Manual::API::V1::ValueTypes>.

A listing of all built-in object types can be found in
L<Zaaksysteem::Manual::API::V1::Types>.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
