package Zaaksysteem::Object::Attribute::TimestampOrText;

use Moose::Role;

use Scalar::Util qw(blessed);

=head2 _build_human_value

Return the human-readable format for this field.

If the value is a DateTime instance, it will be formatted using $self->format,
if it's not, the plain value is returned (it's most likely a plain string).

=cut

sub _build_human_value {
    my $self = shift;

    if (blessed($self->value) && $self->value->isa('DateTime')) {
        return $self->value->strftime($self->format || '%d-%m-%Y') if $self->value;
    }

    return $self->value;
}

sub _build_index_value {
    my $self = shift;
    my ($value) = @_;

    if (blessed($value) && $value->isa('DateTime')) {
        # We return a date that can be exactly matched on in localtime
        # This fixes ZS-5290, so users can query on exact dates for
        # plain attributes.
        return $value->clone->set_time_zone('Europe/Amsterdam')->ymd;
    }

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

