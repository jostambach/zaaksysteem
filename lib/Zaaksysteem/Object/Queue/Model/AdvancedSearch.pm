package Zaaksysteem::Object::Queue::Model::AdvancedSearch;

use Moose::Role;

use BTTW::Tools;
use Zaaksysteem::Search::Object;
use Zaaksysteem::Export::Model;
use Zaaksysteem::Export::CSV;

=head1 NAME

Zaaksysteem::Object::Queue::Model::AdvancedSearch - AdvancedSearch queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 export_advanced_search

=cut

sig export_advanced_search => 'Zaaksysteem::Backend::Object::Queue::Component';

sub export_advanced_search {
    my $self = shift;
    my $item = shift;

    my $data = $item->data;

    my $user = $self->find_subject($data->{subject_id});

    my $om = Zaaksysteem::Object::Model->new(
        table  => 'ObjectData',
        schema => $self->schema,
        user   => $user,
    );

    my $model = Zaaksysteem::Search::Object->new(
        user         => $user,
        schema       => $self->schema,
        query        => $data->{zql},
        object_model => $om,
    );

    my $format = $data->{format};

    my $results = $model->search();

    # TODO: Rename to Export::Format
    my $formatting = Zaaksysteem::Export::CSV->new(
        user              => $user,
        blacklist_mapping => $model->blacklisting,
        objects           => $results,
        format            => $format,
    );

    $formatting->process;

    my $fh = $formatting->to_filehandle();

    my $export = Zaaksysteem::Export::Model->new(
        schema     => $self->schema,
        user       => $user,
        request_id => $item->metadata->{request_id},
        uri        => URI->new_abs('/exportqueue', $self->base_uri),
    );

    # calc is ods.. so yeah
    my $filename = $export->generate_filename(
        'uitgebreid-zoeken', $format eq 'calc' ? 'ods' : $format,
    );

    $export->add_as_export($fh, $filename);
    return;
}

sig export_document_list => 'Zaaksysteem::Backend::Object::Queue::Component';

sub export_document_list {
    my $self = shift;
    my $item = shift;

    my $data = $item->data;

    my $user = $self->find_subject($data->{subject_id});

    my $om = Zaaksysteem::Object::Model->new(
        table  => 'ObjectData',
        schema => $self->schema,
        user   => $user,
    );

    my $model = Zaaksysteem::Search::Object->new(
        user         => $user,
        schema       => $self->schema,
        query        => $data->{zql},
        object_model => $om,
    );

    my $format = 'csv';

    my $results = $model->search_documents();
    my @objects;
    while (my $zaak = $results->next) {
        push(@objects, $zaak->export_files);
    }

    my $formatting = Zaaksysteem::Export::CSV->new(
        user              => $user,
        blacklist_mapping => {},
        objects           => \@objects,
        format            => $format,
        column_order      => [
            qw(
                Bestandsnaam
                Bestandstype
                Aangemaakt
                Reden
                Status
                Documentstatus
                ZaakID
                Versie
            )
        ],
    );

    $formatting->process;

    my $fh = $formatting->to_filehandle();

    my $export = Zaaksysteem::Export::Model->new(
        schema     => $self->schema,
        user       => $user,
        request_id => $item->metadata->{request_id},
        uri        => URI->new_abs('/exportqueue', $self->base_uri),
    );

    my $filename = $export->generate_filename(
        'documentlijst', $format,
    );

    $export->add_as_export($fh, $filename);
    return;
}

sig export_tmlo => 'Zaaksysteem::Backend::Object::Queue::Component';

sub export_tmlo {
    my $self = shift;
    my $item = shift;

    my $data = $item->data;

    my $user = $self->find_subject($data->{subject_id});

    my $om = Zaaksysteem::Object::Model->new(
        table  => 'ObjectData',
        schema => $self->schema,
        user   => $user,
    );

    my $model = Zaaksysteem::Search::Object->new(
        user         => $user,
        schema       => $self->schema,
        query        => $data->{zql},
        object_model => $om,
    );


    my $results = $model->search();

    my $fh = File::Temp->new();
    my $tar = Archive::Tar::Stream->new(outfh => $fh);

    $om->export_multiple(
        resultset  => $results,
        tar_handle => $tar,
        metadata   => {
            user        => $model->user->username,
            environment => $self->environment,
            zs_version  => $self->zs_version,
            query       => $model->query,
        }
    );

    $tar->FinishTar();

    my $export = Zaaksysteem::Export::Model->new(
        schema     => $self->schema,
        user       => $user,
        request_id => $item->metadata->{request_id},
        uri        => URI->new_abs('/exportqueue', $self->base_uri),
    );

    my $filename = $export->generate_filename(
        'overdrachtsbestand', 'tar',
    );

    $export->add_as_export($fh, $filename);
    return;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
