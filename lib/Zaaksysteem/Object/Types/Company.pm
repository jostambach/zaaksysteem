package Zaaksysteem::Object::Types::Company;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw(
    Zaaksysteem::Object::Roles::Relation
    Zaaksysteem::BR::Subject::Types::Company
    Zaaksysteem::Object::Types::Roles::Address
);

use BTTW::Tools;
use Zaaksysteem::Types qw(
    ObjectSubjectType
    JSONBoolean
    CompanyCocNumber
    CompanyCocLocationNumber
    DateTimeObject
);
use Zaaksysteem::BR::Subject::Constants qw/SUBJECT_CONFIGURATION SUBJECT_MAPPING/;

use Zaaksysteem::Object::Types::LegalEntityType;
use Zaaksysteem::Object::Types::CountryCode;
use Zaaksysteem::Object::Types::Company::Activity;

use MooseX::Types::Moose qw(ArrayRef HashRef Str);
use Moose::Util::TypeConstraints;

use Data::Dumper;

class_type 'CompanyActivity', { class => 'Zaaksysteem::Object::Types::Company::Activity' };
coerce 'CompanyActivity', from HashRef, via {
    my $val = $_;
    $val->{code} //= '000';
    $val->{description} = '-';
    return Zaaksysteem::Object::Types::Company::Activity->new(%$val)
};

subtype 'ArrayOfCompanyActivity', as 'ArrayRef[CompanyActivity]';
coerce 'ArrayOfCompanyActivity',
    from 'CompanyActivity',
    via { [ $_ ] };

use constant SUBJECT_TYPE => 'company';
use constant SCHEMA_TABLE => 'Bedrijf';

=head1 NAME

Zaaksysteem::Object::Types::Company - This object describes a subject of the type: Company

=head1 DESCRIPTION

This object contains the primary data of a company, like the "name" (handelsnaam) and their
coc number. It has references to their correspondence and residential address.

=head1 COMPANY ATTRIBUTES

=head2 coc_number

The number for this company from the Chamber of Commerce.

=cut

has coc_number => (
    is       => 'rw',
    isa      => CompanyCocNumber,
    traits   => [qw(OA)],
    label    => 'The CoC number (kvk-nummer)',
);

=head2 coc_location_number

The company-location number for this company from the Chamber of Commerce.

=cut

has coc_location_number => (
    is       => 'rw',
    isa      => CompanyCocLocationNumber,
    traits   => [qw(OA)],
    label    => 'The location of residence number (vestigingsnummer)',
);

=head2 rsin

The RSIN number for this company from the Chamber of Commerce.

=cut

has rsin => (
    is       => 'rw',
    isa      => 'Num',
    traits   => [qw(OA)],
    label    => 'RSIN',
    required => 0,
);

=head2 oin

The OIN number for this company from the Chamber of Commerce.

=cut

has oin => (
    is       => 'rw',
    isa      => 'Num',
    traits   => [qw(OA)],
    label    => 'ION',
    required => 0,
);

=head2 company

The name of the company

=cut

has company => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Company name',
);


=head2 company_type

The type of the.

=cut

has company_type => (
    is       => 'rw',
    isa      => 'Zaaksysteem::Object::Types::LegalEntityType',
    traits   => [qw(OR)],
    embed    => 1,
    label    => 'Company type',
);

=head2 mobile_phone_number

This subject's mobile phone number.

=cut

has mobile_phone_number => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Mobile phone number',
    required => 0,
);

=head2 phone_number

This subject's preferred phone number.

=cut

has phone_number => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Phone number',
    required => 0,
);

=head2 email_address

This subject's email address

=cut

has email_address => (
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    label    => 'Email address',
    required => 0,
);

has main_activity => (
    is       => 'rw',
    isa      => 'CompanyActivity',
    traits   => [qw(OA)],
    label    => 'Activity',
    required => 0,
    coerce   => 1,
);

has secondairy_activities => (
    is       => 'rw',
    isa      => 'ArrayOfCompanyActivity',
    traits   => [qw(OA)],
    label    => 'Secondary activities',
    required => 0,
    coerce   => 1,
);

has date_founded => (
     is       => 'rw',
     isa      => DateTimeObject,
     traits   => [qw(OA)],
     label    => 'Date founded',
     required => 0,
     coerce   => 1,
);

has date_registration => (
    is       => 'rw',
    isa      => DateTimeObject,
    traits   => [qw(OA)],
    label    => 'Date registered',
    required => 0,
    coerce => 1,
);

has date_ceased => (
    is       => 'rw',
    isa      => DateTimeObject,
    traits   => [qw(OA)],
    label    => 'Date ceased',
    required => 0,
    coerce => 1,
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
