package Zaaksysteem::Object::Types::Naw;
use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw(Zaaksysteem::Object::Roles::Relation Zaaksysteem::Object::Roles::Security);

use BTTW::Tools;
use Zaaksysteem::Types qw(Betrokkene);

=head1 NAME

Zaaksysteem::Object::Types::NAW - Built-in object type implementing a class for NAW objects

=head1 DESCRIPTION

NAW objects are objects in which Naam, Adres, Woonplaats information is stored.

=head1 ATTRIBUTES

=head2 owner

The name of the person/organisation

=cut

has owner => (
    is       => 'rw',
    isa      => Betrokkene,
    traits   => [qw[OA]],
    label    => 'The owner of the object type',
    required => 1,
    unique   => 1,
);

=head2 naam

The name of the person/organisation

=cut

has naam => (
    label    => 'Naam',
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    required => 1,
);

=head2 straatnaam

The street name of the organisation

=cut

has straatnaam => (
    label    => 'straatnaam',
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    required => 1,
);

=head2 huisnummer

The huisnummer of the organisation

=cut

has huisnummer => (
    label    => 'huisnummer',
    is       => 'rw',
    isa      => 'Int',
    traits   => [qw(OA)],
    required => 1,
);

=head2 huisnummer_letter

The huisnummer letter, eg A, B or HS.

=cut

has huisnummer_letter => (
    label    => 'huisnummer_letter',
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    required => 0,
);

=head2 huisnummer_toevoeging

The huisnummer_toevoeging

=cut

has huisnummer_toevoeging => (
    label    => 'huisnummer_toevoeging',
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    required => 0,
);

=head2 postcode

A postcode, zipcode

=cut

has postcode => (
    label    => 'postcode',
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    required => 1,
);

=head2 woonplaats

A woonplaats

=cut

has woonplaats => (
    label    => 'woonplaats',
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
    required => 1,
);

=head2 telefoonnummer

A telefoonnumber, telephone number

=cut

has telefoonnummer => (
    label    => 'telefoonnummer',
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
);

=head2 geo addressing

A telefoonnumber, telephone number

=cut

has latitude => (
    label    => 'latitude',
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
);

has longitude => (
    label    => 'longitude',
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
);

=head2 email

An e-mail address

=cut

has email => (
    label    => 'email',
    is       => 'rw',
    isa      => 'Str',
    traits   => [qw(OA)],
);

=head2 new_empty

This instantiator returns a new instance of a generic type, with enough
default variables to make it validate. It can be used to save a temporary
ObjectType that will be edited later. It is also used by the form API for
creating empty forms.

=cut

sub new_empty {
    my $self = shift;

    $self->new();
}

=head2 relatable_types

Returns a list of types that can be related to a NAW object

=cut

sub relatable_types {
    return qw(controlpanel);
}


=head2 new_from_betrokkene

Shortcut for creating this object from a given betrokkene

=cut

define_profile new_from_betrokkene => (
    required => {
        betrokkene  => 'Zaaksysteem::Betrokkene::Object::Bedrijf',
    }
);

sub new_from_betrokkene {
    my $class       = shift;
    my $params      = assert_profile({ @_ })->valid;

    my $betrokkene  = $params->{betrokkene};

    return $class->new(
        owner                   => 'betrokkene-bedrijf-' . $betrokkene->gmid,
        naam                    => $betrokkene->handelsnaam,
        straatnaam              => $betrokkene->vestiging_straatnaam,
        huisnummer              => $betrokkene->vestiging_huisnummer,
        postcode                => $betrokkene->vestiging_postcode,
        woonplaats              => $betrokkene->vestiging_woonplaats,
        $betrokkene->vestiging_huisnummertoevoeging ? (huisnummer_toevoeging => $betrokkene->vestiging_huisnummertoevoeging) : (),
        $betrokkene->vestiging_huisletter ? (huisnummer_letter => $betrokkene->vestiging_huisletter) : (),
        $betrokkene->telefoonnummer ? (telefoonnummer => $betrokkene->telefoonnummer) : (),
        $betrokkene->email ? (email => $betrokkene->email) : (),
    );

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
