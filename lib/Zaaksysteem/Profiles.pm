package Zaaksysteem::Profiles;
use strict;
use warnings;
use utf8;

use Zaaksysteem::Constants;
use Zaaksysteem::Types qw(BSN NonEmptyStr);
use BTTW::Tools;

=head1 NAME

Zaaksysteem::Profiles - Constants for DataForm::Validator profiles used in Zaaksysteem

=head1 SYNOPSIS

use Zaaksysteem::Profiles qw(SOME_PROFILE);
use BTTW::Tools;

define_profile foo => (
    %{ SOME_PROFILE->() }
);

=cut

require Exporter;
our @ISA        = qw/Exporter/;
our @EXPORT     = qw/
    PROFILE_NOTITIE_CREATE
    PROFILE_JOB_CREATE
    PROFILE_DOCUMENT_CREATE
    PROFILE_JOB_UPDATE_KENMERK
    PROFILE_WIJZIG_BETROKKENE
    PROFILE_WIJZIG_ROUTE
    PROFILE_WIJZIG_FASE
    PROFILE_WIJZIG_STATUS
    PROFILE_BULK_UPDATE_SET_SETTINGS
    PROFILE_ZAAK_DOCUMENTS_ADD_MAIL

    ZAPI_PROFILES

    PROFILE_NATUURLIJK_PERSOON
    PROFILE_NATUURLIJK_PERSOON_ADDRESS_PARAMS
    AUTHENTICATED_PARAM
/;

=head1 EXPORTED PROFILES

=over

=item PROFILE_NOTITIE_CREATE

=item PROFILE_JOB_CREATE

=item PROFILE_DOCUMENT_CREATE

=item PROFILE_JOB_UPDATE_KENMERK

=item PROFILE_WIJZIG_BETROKKENE

=item PROFILE_WIJZIG_ROUTE

=item PROFILE_WIJZIG_FASE

=item PROFILE_WIJZIG_STATUS

=item PROFILE_BULK_UPDATE_SET_SETTINGS

=item PROFILE_ZAAK_DOCUMENTS_ADD_MAIL

=item ZAPI_PROFILES

=item PROFILE_NATUURLIJK_PERSOON

=item PROFILE_NATUURLIJK_PERSOON_ADDRESS_PARAMS

=item AUTHENTICATED_PARAM

=back

=cut

use constant 'PROFILE_NOTITIE_CREATE'     => {
    required => [ qw[medium subject_id] ],
    optional => [ qw[content type onderwerp case_id] ],

    msgs => PARAMS_PROFILE_DEFAULT_MSGS,

    constraint_methods => {
        onderwerp => qr/^.{1,256}$/,
        contactkanaal => qr/^\w{1,128}$/,
        case_id => qr/^\d+$/,
    },

    require_some => {
        content_or_onderwerp => [1, qw/content onderwerp/],
    },

    field_filters => {
        medium => ['lc'],
    },
};

use constant 'PROFILE_JOB_CREATE'     => {
    'required'          => [qw/
        bericht
        contactkanaal
        betrokkene
        naam
        opts
        task
        task_context
    /],
    constraint_methods  => {
        'contactkanaal'   => qr/^\w{1,128}$/,
    },
    field_filters       => {
        contactkanaal   => ['lc'],
    },
    msgs                => PARAMS_PROFILE_DEFAULT_MSGS,
};

use constant 'PROFILE_JOB_UPDATE_KENMERK'     => {
    'required'          => [qw/
        kenmerk_id
        betrokkene
        zaak_id
    /],
    'optional'          => [qw/
        kenmerk_value
        toelichting
    /],
    constraint_methods  => {
        'kenmerk_id'        => qr/^\d+$/,
    },
    field_filters       => {
        kenmerk_value       => sub {
            my $field   = shift;

            if (UNIVERSAL::isa($field, 'ARRAY')) {
                my @results;
                for my $value (@$field) {
                    push(@results, ALLOW_ONLY_TRUSTED_HTML->($field));
                }

                return @results;
            }

            return ALLOW_ONLY_TRUSTED_HTML->($field);
        },
        toelichting         => ALLOW_ONLY_TRUSTED_HTML,
    },
    msgs                => PARAMS_PROFILE_DEFAULT_MSGS,
};

use constant 'PROFILE_DOCUMENT_CREATE'     => {
    'required'      => [qw/
        naam

        store_type
    /],
    'optional'      => [qw/
        id
        folder_id
        zaak_id
        milestone
        betrokkene_dsn
        zaaktype_kenmerken_id
        verplicht
        pip
        versie
        filestore_id
        notitie_id
        queue
        ontvangstdatum
        document_type
    /],
    constraint_methods => {
        'onderwerp'   => qr/^.{256}$/,
        'contactkanaal'   => qr/^\w{128}$/,
    },
    require_some        => {
        zaak_or_betrokkene  => [1, qw/betrokkene_dsn zaak_id/],
        minimal_one_store   => [1, qw/filestore_id notitie_id job_id/]
    },
    defaults            => {
        'document_type'     => 'Anders',
        'versie'            => 1,
        'ontvangstdatum'    => sub {
            return DateTime->now();
        },
        'milestone'         => sub {
            my ($dfv) = @_;

            return $dfv->get_filtered_data->{'milestone'}
                if (
                    defined($dfv->get_filtered_data->{'milestone'}) &&
                    $dfv->get_filtered_data->{'milestone'}
                );

            if (
                defined($dfv->get_filtered_data->{'zaak_id'}) &&
                $dfv->get_filtered_data->{'zaak_id'}
            ) {
               return 1;
            }

            return undef;
        }
    },
};

use constant PROFILE_WIJZIG_BETROKKENE  => {
    required    => ['betrokkene_identifier'],
    msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
};

use constant PROFILE_WIJZIG_ROUTE => {
    required => {
        route_ou   => 'Int',
        route_role => 'Int',
    },
    optional => {
        force                    => 'Bool',
        comment                  => NonEmptyStr,
        change_only_route_fields => 'Bool',
        subject                  => 'Zaaksysteem::Object::Types::Subject',
    },
    msgs => PARAMS_PROFILE_DEFAULT_MSGS,
};

use constant PROFILE_WIJZIG_FASE            => {
    required            => [qw/milestone/],
    constraint_methods  => {
        milestone           => qr/^\d+/,
    },
    msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
};

use constant PROFILE_BULK_UPDATE_SET_SETTINGS => {
    required => [qw//],
    optional => [qw/vernietigingsdatum_type vernietigingsdatum/],
    constraint_methods  => {
        vernietigingsdatum_type => sub {
            my ($dfv, $val) = @_;

            my $vernietigingsdatum = $dfv->{__INPUT_DATA}->{vernietigingsdatum};
            if($val eq 'termijn') {
                unless(
                    $vernietigingsdatum =~ /^\d{2}\-\d{2}\-\d{4}$/
                ) {
                    warn "returning undef";
                    return;
                }
            }
            return 1;
        },
        vernietigingsdatum  => sub {
            my ($dfv, $val) = @_;

            if ($val =~ /^\d{2}\-\d{2}\-\d{4}$/) {
                return 1;
            }

            return;
        },
    },
    msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
};


use constant PROFILE_ZAAK_DOCUMENTS_ADD_MAIL => {
    required            => [qw/mailtype rcpt_type/],
    optional            => [qw/medewerker_betrokkene_id zaaktype_notificatie_id/],
    dependencies        => {
       rcpt_type => sub {
            my $dfv  = shift;
            my $type = shift;

            return [ 'medewerker_betrokkene_id' ] if ($type eq 'medewerker');
            return [ ];
        },
       mailtype => sub {
            my $dfv  = shift;
            my $type = shift;

            return [ 'zaaktype_notificatie_id' ] if ($type eq 'bibliotheek_notificatie');
            return [ ];
        },
    },
    msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
};

use constant ZAPI_PROFILES                  => {
    'helloworld.create'       => {
        required        => [qw/
            subject
            description
        /],
        constraint_methods  => {
            subject     => qr/^[\w\s]+$/,
            description => qr/^[\w\s]+$/,
        },
        msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
    },
    'helloworld.update'       => {
        required        => [qw/
            id
            subject
            description
        /],
        constraint_methods  => {
            subject     => qr/^[\w\s]+$/,
            description => qr/^[\w\s]+$/,
        },
        msgs        => PARAMS_PROFILE_DEFAULT_MSGS,
    },
};


=head2 PROFILE_NATUURLIJK_PERSOON

2014 version of Natuurlijk Persoon validation. Only the columns we actually use, StUF
makes already use of this profile.

=cut

use constant PROFILE_NATUURLIJK_PERSOON => {
    'missing_optional_valid'  => 1,
    'required'      => [qw/
        geslachtsnaam


        geboortedatum

        functie_adres

        aanduiding_naamgebruik
    /],
    'optional'      => [qw/
        burgerservicenummer
        geslachtsaanduiding
        a_nummer
        voornamen
        voorletters
        voorvoegsel

        adellijke_titel

        straatnaam
        huisnummer
        postcode
        huisnummertoevoeging
        huisletter
        woonplaats
        gemeente_code
        landcode
        adres_buitenland1
        adres_buitenland2
        adres_buitenland3

        burgerlijke_staat

        indicatie_geheim

        onderzoek_persoon
        onderzoek_huwelijk
        onderzoek_overlijden
        onderzoek_verblijfplaats

        datum_overlijden

        partner_a_nummer
        partner_voorvoegsel
        partner_geslachtsnaam
        partner_burgerservicenummer

        in_gemeente
    /],
    constraint_methods => {
        geslachtsaanduiding => qr/^[MV]?$/,
        burgerservicenummer => sub { BSN->check(pop) },
        functie_adres       => qr/^B|W$/,

        ### From StUF
        a_nummer            => qr/^[1-9][0-9]{9}$/,
        voornamen           => qr/^.{1,200}$/,
        voorletters         => qr/^.{1,20}$/,           ### 10 in 0204, 20 in 0301
        voorvoegsel         => qr/^.{1,10}$/,
        geslachtsnaam       => qr/^.{1,200}$/,
        indicatie_geheim    => qr/^\d$/,
        burgerlijke_staat   => qr/^[0-7]$/,
        aanduiding_naamgebruik => qr/^[ENPV]$/,
        huisnummer          => qr/^\d+$/,
        huisnummertoevoeging => qr/^[a-zA-Z0-9]{0,4}$/,
        huisletter          => qr/^[a-zA-Z]$/,
    },
    defaults        => {
        'functie_adres'             => 'W',
        'aanduiding_naamgebruik'    => 'E',
        'landcode'                  => 6030,
    },
    dependencies => {
        landcode => sub {
            my $dfv     = shift;
            my $code    = shift;

            if ($code eq '6030') {
                return ['postcode','huisnummer','straatnaam','woonplaats'];
            } else {
                return ['adres_buitenland1'];
            }
        },
        authenticated => sub {
            return [ qw(burgerservicenummer) ]
        },
    },

    field_filters     => {
        'huisletter'            => sub { uc(shift) },
        'postcode'    => sub {
            my ($field) = @_;

            $field = uc($field);
            $field =~ s/\s*//g;

            return $field;
        },
        'geslachtsaanduiding'       => sub {
            my ($field) = @_;

            return $field unless $field =~ /^[mMvV]?$/;

            return uc($field);
        },
        'partner_burgerservicenummer' => sub {
            my ($field) = @_;

            return '' if $field =~ /^[0 ]+$/;

            return $field;
        },
    },
};

use constant PROFILE_NATUURLIJK_PERSOON_ADDRESS_PARAMS => [qw/
    straatnaam
    huisnummer
    postcode
    huisnummertoevoeging
    huisletter
    woonplaats
    functie_adres
    gemeente_code
/];

use constant AUTHENTICATED_PARAM => qr/^medewerker|digid|gba$/;

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015,2017 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
