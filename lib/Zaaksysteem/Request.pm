package Zaaksysteem::Request;

use Moose;
use Data::Dumper;

extends qw/Catalyst::Request/;

sub _is_xhr_requested_with {
    my $self = shift;

    return
        $self->header('x-requested-with') &&
        $self->header('x-requested-with') eq 'XMLHttpRequest';
}

sub _is_xhr_accept_json {
    my $self = shift;

    my @accept_types = split m[,\s*], ($self->header('Accept') || '');

    return unless scalar(@accept_types);

    return shift @accept_types eq 'application/json';
}

sub is_xhr {
    my $self = shift;

    return $self->_is_xhr_requested_with || $self->_is_xhr_accept_json;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 is_xhr

TODO: Fix the POD

=cut

