package Zaaksysteem::Roles::Timer;
use Moose::Role;
use namespace::autoclean;

requires qw(log);

=head1 NAME

Zaaksysteem::Roles::Timer - Implement Zaaksysteem::Timer in a Moose role

=head1 SYNOPSYS

    package Zaaksysteem::Random;
    use Moose;
    use namespace::autoclean;

    with qw(
        MooseX::Log::Log4perl
        Zaaksysteem::Roles::Timer
    );

    sub foo {
        my $self = shift;
        $start = $self->timer->start;
        # do things
        my $duration = $self->timer->stop;
        $self->log->trace("It took me $duration ms to do things);
    }

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 timer

A Zaaksysteem::Timer object.

=cut

use Zaaksysteem::Timer;

has timer => (
    is => 'ro',
    isa => 'Zaaksysteem::Timer',
    lazy => 1,
    default => sub {
        return Zaaksysteem::Timer->new();
    },
);

=head1 METHODS

=head2 log_start

Log the start time (buggy)

=cut

sub log_start {
    my $self = shift;
    $self->_log(shift, scalar $self->timer->start);
    return $self->timer->start;
}

=head2 log_stop

Log the duration and reset the timer

=cut

sub log_stop {
    my $self = shift;
    $self->_log(shift, $self->timer->stop(shift));
    return $self->timer->start;
}


=head2 log_time

Log the duration and continue the timer

=cut

sub log_time {
    my $self = shift;
    $self->_log(shift, $self->timer->time(shift));
    return $self->timer->_starttime;
}

sub _log {
    my ($self, $msg, $time) = @_;
    return unless $self->log->is_trace;
    $self->log->trace("$msg took $time ms");
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
