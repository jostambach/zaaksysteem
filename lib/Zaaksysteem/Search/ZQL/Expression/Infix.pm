package Zaaksysteem::Search::ZQL::Expression::Infix;

use Moose;

use Zaaksysteem::Search::Conditional;

extends 'Zaaksysteem::Search::ZQL::Expression';

has operator => ( is => 'ro', required => 1 );
has lterm => ( is => 'ro', required => 1 );
has rterm => ( is => 'ro', required => 1 );

sub new_from_production {
    my $class = shift;
    my %item = @_;

    return $class->new(
        lterm => $item{ term },
        operator => $item{ infix_operator },
        rterm => $item{ 'infix_expression_tail(1)' }[0]
    );
}

sub dbixify {
    my $self = shift;
    my $cmd = shift;

    return Zaaksysteem::Search::Conditional->new(
        lterm => $self->lterm->dbixify($cmd),
        operator => $self->operator->dbixify($cmd),
        rterm => $self->rterm->dbixify($cmd)
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 dbixify

TODO: Fix the POD

=cut

=head2 new_from_production

TODO: Fix the POD

=cut

