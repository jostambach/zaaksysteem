package Zaaksysteem::XML::MijnOverheid::InstanceV2;
use Moose;

use File::Spec::Functions qw(catfile);

with 'Zaaksysteem::XML::Compile::Instance';

has name => (
    is => 'ro',
    default => "mijnoverheid_v2",
);

has schemas => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;

        return [
            catfile($self->home, "share/xsd/mijnoverheid/zaaksysteem_berichtenbox_check_v2.xsd"),
            catfile($self->home, "share/xsd/mijnoverheid/zaaksysteem_berichtenbox_v2.xsd"),
            catfile($self->home, "share/xsd/mijnoverheid/zaaksysteem_lopendezaken.xsd"),
        ];
    }
);

has elements => (
    is => 'ro',
    lazy => 1,
    default => sub {
        return [
            {
                element => '{http://www.zaaksysteem.nl/xml/mijnoverheid/berichtenbox-v2}berichtenbox_message',
                compile => 'RW',
                method  => 'berichtenbox_message',
            },
            {
                element => '{http://www.zaaksysteem.nl/xml/mijnoverheid/berichtenbox-v2}berichtenbox_message_response',
                compile => 'RW',
                method  => 'berichtenbox_message_response',
            },
            {
                element => '{http://www.zaaksysteem.nl/xml/mijnoverheid/berichtenbox-check-v2}has_berichtenbox',
                compile => 'RW',
                method  => 'has_berichtenbox',
            },
            {
                element => '{http://www.zaaksysteem.nl/xml/mijnoverheid/berichtenbox-check-v2}has_berichtenbox_response',
                compile => 'RW',
                method  => 'has_berichtenbox_response',
            },
            {
                element => '{http://www.zaaksysteem.nl/xml/mijnoverheid/lopende-zaken-v1}lopende_zaak',
                compile => 'RW',
                method  => 'lopende_zaak',
            },
            {
                element => '{http://www.zaaksysteem.nl/xml/mijnoverheid/lopende-zaken-v1}lopende_zaak_response',
                compile => 'RW',
                method  => 'lopende_zaak_response',
            },
        ];
    },
);

has _reader_config => (is => 'ro', default => sub {});

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

