package Zaaksysteem::Zaken::Roles::Authorization;

use Moose::Role;

=head1 NAME

Zaaksysteem::Zaken::Roles::Authorization - Permission check infrastructure

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use Clone qw(clone);
use List::Util qw(any uniq);
use Zaaksysteem::Constants qw(ZAAKSYSTEEM_CASE_ACTION_AUTHORIZATION_MATRIX);
use Zaaksysteem::Types qw( ACLCapability ACLEntityType ACLScope);

=head1 METHODS

=head2 check_user_permissions

Checks that the given L<subject|Zaaksysteem::Backend::Subject::Component>
has the requested permissions.

    if ($zaak->check_user_permissions($c->user, 'zaak_read', ...)) {
        ...
    } else {
        barf;
    }

=cut

sig check_user_permissions => 'Zaaksysteem::Backend::Subject::Component, @Str';

sub check_user_permissions {
    my ($self, $subject, @permissions) = @_;

    $self->log->debug(sprintf(
        'Checking "%s" permissions for user "%s" (%s) on case %s',
        join(',', @permissions),
        $subject->display_name,
        $subject->uuid,
        $self->id
    ));

    return 1 if $subject->has_legacy_permission('admin');

    if (any { $_ eq 'zaak_read' || $_ eq 'zaak_search' } @permissions) {
        return 1 if $self->ddd_user_is_staff($subject);
    }

    if (any { $_ eq 'zaak_edit' || $_ eq 'zaak_search' } @permissions) {
        return 1 if $self->ddd_user_is_assignee($subject);
        return 1 if $self->ddd_has_assignee
                 && $self->ddd_user_is_coordinator($subject);
    }

    return 1 if $self->_check_user_casetype_permissions($subject, @permissions);
    return 1 if $self->_check_user_case_permissions($subject, @permissions);

    return 0;
}

sub _check_user_casetype_permissions {
    my ($self, $subject, @permissions) = @_;

    my $zta_rs = $self->zaaktype_id->zaaktype_authorisations->search({
        confidential => ($self->confidentiality eq 'confidential' ? 1 : 0),
        recht => \@permissions,
        ou_id => [ map { $_->id } @{ $subject->parent_groups } ],
        role_id => [ map { $_->id } @{ $subject->roles } ]
    });

    return $zta_rs->count ? 1 : 0;
}

sub _check_user_case_permissions {
    my ($self, $subject, @permissions) = @_;

    my %map = (
        zaak_read   => 'read',
        zaak_search => 'search',
        zaak_edit   => 'write',
        zaak_beheer => 'manage'
    );

    my $exists = $self->zaak_authorisations->search_rs(
        {
            '-or' => [
                {
                    entity_id   => $subject->position_matrix,
                    entity_type => 'position',
                },
                {
                    entity_id   => $subject->username,
                    entity_type => 'user',
                }
            ],
            capability => [map { $map{$_} } @permissions],
            scope => 'instance',
        },
        { rows => 1 },
    )->first;

    return $exists ? 1 : 0;
}

sub _eval_case_action_authorization_matrix {
    my ($self, $user, $user_perm_checker, $context, $action) = @_;

    my $matrix = ZAAKSYSTEEM_CASE_ACTION_AUTHORIZATION_MATRIX;
    my $results = {};

    # First eval default context
    if ($context ne '_' and my $actions = $matrix->{ _ }) {
        my $_results = {};

        if (my $rules = $actions->{ _ }) {
            $_results->{ _ } = $self->_assert_auth_rules(
                $rules,
                $user,
                $user_perm_checker
            );
        }

        if (my $rules = $actions->{ $action }) {
            $_results->{ $action } = $self->_assert_auth_rules(
                $rules,
                $user,
                $user_perm_checker
            );
        }

        $results->{ _ } = $_results;
    }

    my $actions = $matrix->{ $context };
    my $_results = {};

    unless (defined $actions) {
        throw('zaak/authorization/context_does_not_exist', sprintf(
            'Requested case authorization context "%s" does not exist.',
            $context
        ));
    }

    if (my $rules = $actions->{ _ }) {
        $_results->{ _ } = $self->_assert_auth_rules(
            $rules,
            $user,
            $user_perm_checker
        );
    }

    my $rules = $actions->{ $action };

    unless (defined $rules) {
        throw('zaak/authorization/action_does_not_exist', sprintf(
            'Requested case authorization action "%s" does not exist in context "%s"',
            $action,
            $context
        ));
    }

    $_results->{ $action } = $self->_assert_auth_rules(
        $rules,
        $user,
        $user_perm_checker
    );

    $results->{ $context } = $_results;

    return $results;
}

sub _assert_auth_rules {
    my ($self, $rules, $user, $user_perm_checker) = @_;

	for my $rule (@{ $rules }) {
        if (my $scope = $rule->{ scope }) {
            next unless $user_perm_checker->($self, @{ $scope });
        }

        my $cond_fail;

        for my $condition (@{ $rule->{ conditions } }) {
            my $predicate = $self->can(sprintf('ddd_%s', $condition));

            $predicate //= $self->_get_auth_predicate($condition);

            unless (defined $predicate) {
                throw('zaak/authorization/predicate_does_not_exist', sprintf(
                    'Given authorization rule condition "%s" does not have a predicate',
                    $condition
                ));
            }

            next if $predicate->($self, $user);

            $cond_fail = 1;

            # Short-circuit rest of tests
            last;
        }

        next if $cond_fail;

        return sprintf(
            'in scope(%s), conditions(%s)',
            join(',', @{ $rule->{ scope } || [] }),
            join(',', @{ $rule->{ conditions } || [] })
        );
    }

    throw('zaak/authorization/user_not_authorized', sprintf(
        'Authorization condition(s) failed for user "%s" on case %s',
        $user->display_name,
        $self->id
    ));
}

sub _get_auth_predicate {
    # Empty for now, this is the intended hook for predicates that
    # don't resolve to a 'ddd_my_predicte' method, or need some other logic
    # for its retrieval.
    #
    # Implementors of this hook should return a subref that takes a case and a
    # user object, and returns a bool-ish value.
}

=head2 update_user_acl

Update ACL's for a user.

=cut

sig update_user_acl => 'HashRef, ?HashRef';

sub update_user_acl {
    my ($self, $acl_value, $cascade_to) = @_;

    # Validate each given acl, and split the capabilities
    my @acls = _preprocess_acl_data([$acl_value],
        sub {
            my $acl = shift;
            if (   $acl->{scope} eq 'instance'
                && $acl->{entity_type} eq 'user')
            {
                return 1;
            }
            return 0;
        }
    );

    my @zaken = $self->_cascade_case_relations($cascade_to);

    @zaken = uniq @zaken;

    # Push the changes into our database
    try {
        $self->result_source->schema->txn_do(sub {
            for my $zaak (@zaken) {
                $self->log->trace(
                    sprintf("Updating user in ZaakAuthorisation for '%s'",
                        $zaak->id)
                );

                # We fancy in place updates, let's compare.
                my $current_acls = $zaak->zaak_authorisations->search_rs(
                    {
                        entity_id   => $acl_value->{entity_id},
                        scope       => 'instance',
                        entity_type => 'user',
                    }
                );
                $zaak->_update_acl($current_acls, @acls);
            }
        });
    } catch {
        $self->log->error("Error updating ACLs: $_");
        throw('case/acl/update/db_error', sprintf(
            'Problem inserting given data in our database'
        ));
    };

    return;
}

=head2 update_group_acls

Update ALL ACL's for a list of groups. Removes all ACL's that do not match the
given ACL

=cut

sig update_group_acls => 'ArrayRef, ?HashRef';

sub update_group_acls {
    my ($self, $acl_values, $cascade_to) = @_;

    # Validate each given acl, and split the capabilities
    my @acls = _preprocess_acl_data(
        $acl_values,
        sub {
            my $acl = shift;
            if (   $acl->{scope} eq 'instance'
                && $acl->{entity_type} eq 'position')
            {
                return 1;
            }
            return 0;
        }
    );
    my @zaken = $self->_cascade_case_relations($cascade_to);

    # Push the changes into our database
    try {
        $self->result_source->schema->txn_do(sub {
            for my $zaak (@zaken) {
                $self->log->trace(
                    sprintf("Updating group in ZaakAuthorisation for '%s'",
                        $zaak->id)
                );

                # We fancy in place updates, let's compare.
                my $current_acls = $zaak->zaak_authorisations->search(
                    {
                        scope       => 'instance',
                        entity_type => 'position',
                    }
                );
                $zaak->_update_acl($current_acls, @acls);
            }
        });
    } catch {
        $self->log->error("Error updating ACLs: $_");
        throw('case/acl/update/db_error', sprintf(
            'Problem inserting given data in our database'
        ));
    };

    return;
}

=head2 _preprocess_acl_data

Pre-process ACL data from API/v1 into a format that C<update_specific_acls> can use.

=cut

sub _preprocess_acl_data {
    my $raw_acl_values = shift;
    my $code_ref = shift;

    my @acls;
    for my $rawacl (@$raw_acl_values) {
        my $rawacl = assert_profile($rawacl, profile => {
            required => {
                entity_id    => 'Str',
                entity_type  => ACLEntityType,
                scope        => ACLScope,
            },
            optional => {
                capabilities => ACLCapability,
            }

        })->valid;

        if ($code_ref) {
            next unless $code_ref->($rawacl);
        }

        my $capabilities = $rawacl->{capabilities};

        if (!$capabilities) {
            push(@acls, _raw_to_acl($rawacl));
            next;
        }

        if (any { $_ eq 'manage' } @$capabilities) {
            $capabilities = [qw(manage write read search)];
        }
        elsif (any { $_ eq 'write' } @$capabilities) {
            $capabilities = [qw(write read search)];
        }
        elsif (any { $_ eq 'read' } @$capabilities) {
            $capabilities = [qw(read search)];
        }
        elsif (any { $_ eq 'search' } @$capabilities) {
            $capabilities = [qw(search)];
        }

        for my $capability (@$capabilities) {
            push(@acls, _raw_to_acl($rawacl, $capability));
        }
    }

    return @acls;
}

sub _raw_to_acl {
    my ($raw, $capability) = @_;
    my $acl = clone $raw;
    $acl->{capability} = $capability;
    delete($acl->{capabilities});
    return $acl;
}

sub _update_acl {
    my ($self, $current_acls, @acls) = @_;
    my %got_indexes;
    DB_ACL:
    while (my $dbacl = $current_acls->next) {

        ACL:
        for (my $i = 0; $i < @acls; $i++) {
            my $acl = $acls[$i];

            if ($dbacl->capability eq $acl->{capability} &&
                $dbacl->entity_id eq $acl->{entity_id} &&
                $dbacl->entity_type eq $acl->{entity_type} &&
                $dbacl->scope eq $acl->{scope}
            ) {
                $got_indexes{$i} = 1;
                next DB_ACL;
            }
        }
        $dbacl->delete;
    }

    # Create the entries not already in the DB
    for (my $i = 0; $i < @acls; $i++) {
        next if $got_indexes{$i};
        my $acl = $acls[$i];
        next unless $acl->{capability}; # Don't add removed ACL's

        $self->zaak_authorisations->create({
            %$acl,
            zaak_id => $self->id,
        });
    }

    # Prepare a delayed touch for the (now modified) case so the ZaakAuthorisation
    # records are synced to object ACLs
    $self->update_object_acl;
}

sub _cascade_case_relations {
    my ($self, $cascade_to) = @_;
    my @zaken = $self;
    if ($cascade_to) {
        if ($cascade_to->{related}) {
            $self->log->trace("Cascade to related cases");
            push @zaken, map {
                $_->case
            } $self->zaak_relaties;
        }

        if ($cascade_to->{partial}) {
            $self->log->trace("Cascade to partial cases");
            push @zaken, $self->zaak_children;
        }

        if ($cascade_to->{continuation}) {
            $self->log->trace("Cascade to continuation cases");
            push @zaken, $self->zaak_vervolgers;
        }
    }
    return @zaken;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
