package Zaaksysteem::Zaken::Roles::Export;
use Moose::Role;

=head1 METHODS

=head2 export_files

Export files for documentenlijst. Search results > Exporteren > Documentenlijst

=cut

sub export_files {
    my $self = shift;

    my @res;
    my $files = $self->active_files;
    while (my $file = $files->next) {
        if ($file->get_column('filestore_id') && !$file->filestore) {
            # virus
            next;
        }

        push(@res, [
            $file->filepath,
            $file->filestore->mimetype,
            $file->date_created->strftime('%d-%m-%Y %H:%M:%S'),
            'zaakbehandeling',
            $self->status eq 'open' ? 'Open' : 'Gearchiveerd',
            $file->document_status,
            $self->id,
            $file->version,
        ]);
    }
    return @res;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
