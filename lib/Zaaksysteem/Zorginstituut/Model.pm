package Zaaksysteem::Zorginstituut::Model;
use Moose;
with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Zorginstituut::Model - The base module for Zorginstituut models

=head1 DESCRIPTION

This instance should not be instantiated but extended.

=head1 SYNOPSIS

    package Foo;
    use Moose;

    extends 'Zaaksysteem::Zorginstituut::Model';

=cut

use Zaaksysteem::Constants qw(RGBZ_GEMEENTECODES);
use BTTW::Tools qw(throw sig);
use UUID4::Tiny qw(create_uuid_string);

=head1 ATTRIBUTES

=head2 schema

A L<Zaaksysteem::Schema> object, required.

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

=head2 interface

A Zaaksysteem::Model::DB::Interface> object, required.

=cut


has interface => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Model::DB::Interface',
    required => 1,
);

=head2 message_version

Message version, consumers must define this attribute

=cut

has message_version => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

=head2 message_version

Message sub version, consumers must define this attribute

=cut

has message_sub_version => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

=head2 municipality_code

The municipality code. Required.

=cut

has municipality_code => (
    is       => 'ro',
    isa      => 'Int',
    required => 1,
);

=head2 municipality_name

The name of the municipality. Automaticly generated from the
C<municipality_code>

=cut

has municipality_name => (
    is      => 'ro',
    isa     => 'Str',
    builder => '_build_municipality_name',
    lazy    => 1,
);

=head2 provider

An instance of an L<Zaaksysteem::Zorginstituut::Provider> class, eg
L<Zaaksysteem::Zorginstituut::Provider::Acknowledge>. Required.

=cut

has provider => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Zorginstituut::Provider',
    required => 1,
);

=head1 METHODS

=head2 label

Displays a human readable label for the model

=cut

sub label {
    my $self = shift;
    return
        sprintf("%s (%s %s)", $self->description, $self->type,
        $self->version);
}

=head2 shortname

Displays a short identitifier for the module

=cut

sub shortname {
    my $self = shift;
    return join("_", lc($self->type), $self->version);
}

=head2 type

The type of model

=cut

sub type {
    my $self = shift;

    return (split(/::/, blessed $self ? blessed $self: $self))[-1];
}

=head2 version

The full version of the message

=cut

sub version {
    my $self = shift;

    my $ver = (split(/::/, blessed $self ? blessed $self: $self))[-2];
    if ($ver =~ /(\d{2})(\d+)/) {
        return "" . Perl::Version->new("$1.$2");
    }
    return $ver;
}

=head2 find_case_by_reference

Find an open case by the reference in the object subscription table.
Dies if we cannot find an open case.

=cut

sig find_case_by_reference => 'Str';

sub find_case_by_reference {
    my ($self, $uuid) = @_;

    my $rs = $self->schema->resultset('ObjectSubscription')->search_rs(
        {
            external_id  => $uuid,
            local_table  => 'Zaak',
            interface_id => $self->interface->id,
        }
    );

    my $os = $rs->next;

    if (!$os) {
        throw(
            'Zorginstituut/reference/notfound',
            "$uuid cannot be matched against a case"
        );
    }
    elsif ($rs->next) {
        throw(
            'Zorginstituut/reference/duplicate',
            "$uuid is linked to multiple cases"
        );
    }

    my $case = $self->schema->resultset('Zaak')->find(
        $os->get_column('local_id')
    );

    return $case if $case && !$case->is_afgehandeld;

    throw(
        'Zorginstituut/reference/no_open_case',
        "No open case can be found by object_subscription: $uuid",
    );
}

=head2 create_object_subscription

Create an object subscription for a case

=cut

sig create_object_subscription => 'Zaaksysteem::Schema::Zaak';

sub create_object_subscription {
    my ($self, $case) = @_;

    return $self->schema->resultset('ObjectSubscription')->create(
        {
            external_id  => create_uuid_string(),
            local_id     => $case->id,
            local_table  => 'Zaak',
            interface_id => $self->interface->id,
        },
    );
}

=head2 supports_soap_action

Check if the given SOAPAction is supported by this module.

=cut

sub supports_soap_action {
    my ($self, $action) = @_;

    if (($action // '') eq 'http://www.stufstandaarden.nl/koppelvlak/ggk0210/ggk_Du01') {
        return 1
    }
    return 0;
}

=head2 get_case

Get the case from based on the cross reference from the XML

=cut

sig get_case => 'XML::LibXML::XPathContext, =>Zaaksysteem::Schema::Zaak';

sub get_case {
    my ($self, $du01) = @_;

    my $ref  = $du01->findvalue('//s:crossRefnummer');
    $self->log->trace("Found reference $ref");
    return $self->find_case_by_reference($ref);
}

=head2 process_soap

Process the SOAP message, the actual logic to process and log to the case

=cut

sig process_soap => 'Str, =>Bool';

sub process_soap {
    my ($self, $xml) = @_;

    my $du01      = $self->get_stuf_du01($xml);
    my $case      = $self->get_case($du01);
    my $validator = $self->du01_to_xml($du01);

    $self->log_to_case($case,
        type => $self->answer_type,
        $validator->valid ? ( ) : ( errors => $validator->errors ),
    );

    return $validator->valid;
}

sub _build_municipality_name {
    my $self = shift;

    my $city = RGBZ_GEMEENTECODES->{int($self->municipality_code)};
    return $city if $city;

    throw('zorginstituut/no_valid_municipality_code',
        "Invalid municipality code found: " . $self->municipality_code);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
