package Zaaksysteem::Zorginstituut::Model::0201::JW301;
use Moose;

extends 'Zaaksysteem::Zorginstituut::Model::0201';
with qw(
    Zaaksysteem::Zorginstituut::Roles::Plugin
);

=head1 NAME

Zaaksysteem::Zorginstituut::0201::JW301 - Rule the JW301 process

=head1 DESCRIPTION

Implement the logic for the JW301 process, send JW301, receive JW302.

=head1 SYNOPSIS

=cut

use MIME::Base64 qw(decode_base64);
use XML::LibXML::XPathContext;
use XML::LibXML;

use Zaaksysteem::Constants qw(LOGGING_COMPONENT_ZAAK);

use BTTW::Tools;
use Zaaksysteem::Zorginstituut::Messages::0201::JW301;
use Zaaksysteem::Zorginstituut::Reader;

has '+message_version' => ( default => 2 );
has '+message_sub_version' => ( default => 1 );

=head1 METHODS

=head2 code

The JW message type: 436

=cut

sub code {
    my $self = shift;
    return 436;
}

=head2 description

The description of the process this model implements.

=cut

sub description {
    return "Toewijzing Jeugd";
}

=head2 answer_type

The type of answer message, for JW301 this is JW302

=cut

sub answer_type {
    return "JW302";
}

=head2 du01_to_xml

Get the base64 encoded message from the du01 stuf message and return the reader

=cut

sub du01_to_xml {
    my ($self, $du01) = @_;

    my $decoded = decode_base64($du01->findvalue('//w:bericht/w:xmlBestand'));
    my $wmo302 = Zaaksysteem::Zorginstituut::Reader->new(
        base_namespace => 'http://www.istandaarden.nl/ijw/2_1/jw302/basisschema/2_1',
        msg_namespace  => 'http://www.istandaarden.nl/ijw/2_1/jw302/schema/2_1',
        code           => 437,
        xml            => $decoded,
    );

    $wmo302->assert_code_valid;
    return $wmo302;
}

sub _build_message_type {
    my ($self, $case) = @_;

    return Zaaksysteem::Zorginstituut::Messages::0201::JW301->new(
        schema              => $self->schema,
        message_version     => $self->message_version,
        message_sub_version => $self->message_sub_version,

        interface         => $self->interface,
        municipality_code => $self->municipality_code,
        provider          => $self->provider,

        case => $case,
    );
}

=head2 get_agbcode_from_301

Get the AGB code from the 301 message

=cut


sub get_agbcode_from_301 {
    my ($self, $xml) = @_;

    my $xp = $self->_get_xpath($xml);

    $xp->registerNs('base', 'http://www.istandaarden.nl/ijw/2_1/basisschema/schema/2_1');
    $xp->registerNs('msg', 'http://www.istandaarden.nl/ijw/2_1/jw301/schema/2_1');

    return $xp->findvalue('//msg:Header/msg:Ontvanger');
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
