[% BLOCK import_dependency_group %]
<div class="import_dependency import_group ezra_[% dependency_type %]">
    <div class="ezra_import_dependency_group_inner">
        <div class="import_title">
            <h3 class="[% dependency_type %]">[% dependency_titles.$dependency_type || dependency_type %]<span></span></h3>
            [%- IF dependency_type == 'BibliotheekKenmerken' -%]
            <div>
                <div class="import_attribute_warnings">
                    <span>
                        <font color="#424242"><B>Let op</B></font>
                        Controleer de bestaande kenmerken na het
                        importeren. Let daarbij in het bijzonder op
                        de checkbox &#39;Gevoelig gegeven&#39;, deze wordt niet
                        ge&iuml;mporteerd.<a target="_blank"
                        rel="noopener"
                        href="https://wiki.zaaksysteem.nl/wiki/Redirect_Catalogus_importeren#zaaksysteem.nl">
                        Meer informatie</a>
                    </span>
                </div>
                <div class="import_kenmerk_only_active_values_div-avg">
                <div class="import_kenmerk_only_active_values_div">
                    <span>
                        <input type="checkbox" id="only_import_active_fieldvalues"
                        [% IF only_import_active_fieldvalues %]checked="checked"[% END %]
                        name="only_import_active_fieldvalues" />
                        <label>Alleen actieve kenmerkwaarden importeren</label>
                    </span>
                </div>
                </div>
            </div>
            [% END %]
        </div>
        <div class="import_group_inner">
            <div class="spinner-groot"><div></div></div>
            [% ids = import_dependencies.$dependency_type %]
            [% FOREACH id = ids.keys %]
                [% dependency_item = ids.$id %]
                <div class="import_dependency_item  import_item">
                [% PROCESS beheer/zaaktypen/import/item.tt %]
                </div>
            [% END %]
        </div>
    </div>
</div>
[% END %]

<style>

.import_dependency_adjustment {
    display: none;
}
.ajaxreturncontainer { 
    display: none; 
}
.import_mintloader {
    width: 400px;
}
</style>

<div class="block clearfix import">
    <div class="block-header block-header-border clearfix">
        <div class="block-header-title">Zaaktype importeren</div>
        <div class="block-header-actions"><a href="/beheer/zaaktypen/import/flush" class="button button-secondary">Ander bestand importeren</a></div>
    </div>
    <div class="block-content">
        [% IF !zaaktype %]
        <form method="POST" enctype="multipart/form-data" class="zaaktype_import" action="/beheer/zaaktypen/import/upload">
            <input type="hidden" name="upload" value="1"/>
            <table class="marginbottom">
                <tr>
                <td class="td250">
                <strong>Selecteer een [% IF zaaktype %]ander [% END %]zaaktype bestand (.ztb):</strong>
                </td>
            
                <td><strong style="color:red;">[% dependency_error %]</strong>
                    [% PROCESS widgets/general/mintloader.tt 
                        veldoptie_name = 'file'
                        send_to_upload = 1
                    %]
                </td>

                
                
                <td>
                <div class="validator rounded invalid" [% IF error %]style="display:block;"[% END %]>
                
                    <div class="validate-tip"></div>
                    <div class="validate-content rounded-right">
                        <span></span>[% error %]
                    </div>
                
                </div>
                </td>
                </tr>
                </table>

                <div class="form-actions form-actions-sticky clearfix">
                 <input type="submit" class="ezra_import_do_upload button button-primary right" value="Volgende"/>
                </div>
                
            </div>
        [% ELSE %]
        <form method="POST" enctype="multipart/form-data" class="zaaktype_import" action="/beheer/zaaktypen/import">      
            <br>        
            <h3>
                Geselecteerd zaaktype: <strong>[% zaaktype.filename %]</strong>
            </h3>

            <div class="import-uitleg"><div>Een zaaktype maakt gebruik van een aantal elementen zoals kenmerken en sjablonen. Om te voorkomen dat bij een import elementen dubbel in het systeem komen kun je gebruik maken van elementen die al aanwezig zijn.
            Hierdoor zijn er meer zoekmogelijkheden en blijft het systeem overzichtelijk. 
            Per element hoef je maar &eacute;&eacute;n keer op te geven hoe het geimporteerd moet worden.
            Hieronder staat een overzicht van alle elementen die het zaaktype gebruikt. Geef per element op of er een bestaand element of het meegeleverde nieuwe element gebruikt moet worden.
            [% IF zaaktype.node.properties.is_casetype_mother %]
                <br/><br/><strong>Let op</strong>: Het importeren van een moederzaaktype leidt niet direct tot wijzigingen in eventuele kindzaaktypen. Publiceer na de import het moederzaaktype om de kindzaaktypen te actualiseren.
            [% END %]
            </div>

            </div>
            <br/>

            [% dependency_titles = {
                BibliotheekSjablonen => 'Sjablonen',
                BibliotheekCategorie => 'Categorieen',
                BibliotheekKenmerken => 'Kenmerken',
                LdapRole             => 'Rol',
                LdapOu               => 'Organisatorische eenheid',
                Zaaktype             => 'Zaaktypen',
                ObjectData           => 'Object',
                ZaaktypeStandaardBetrokkenen => 'Betrokkene',
            } %]

            [% PROCESS import_dependency_group
                dependency_type = 'Zaaktype'
            %]
            [% FOREACH dependency_type = import_dependencies.keys.sort %]
                [% UNLESS dependency_type == 'Zaaktype' || dependency_type == 'BibliotheekCategorie' %]
                    [% PROCESS import_dependency_group %]
                [% END %]
            [% END %]
            <div class="form-actions form-actions-sticky clearfix">
                <input type="submit" name="import" class="button button-primary right" value="Importeren"/>
            </div>
        [% END %]


        </form> 
    </div>
</div>
