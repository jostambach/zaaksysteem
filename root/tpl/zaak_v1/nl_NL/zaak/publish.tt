[% USE Dumper %]
[% USE Scalar %]



[% BLOCK show_documents %]
    [% WHILE (document = documents.next) %]
        <li>
            [% IF document.zaaktype_kenmerken_id %]
                [% document.zaaktype_kenmerken_id.bibliotheek_kenmerken_id.naam %]:
            [% END %]
            [% document.filename %]
        </li>
    [% END %]
[% END %]

[% BLOCK case_documents %]
    [% first_case_doc = 1 %]
    [% FOREACH relation_view = case.zaak_relaties %]
        [% first_case_doc = 0 %]
        [% subcase = relation_view.case %]
        [% active_files = subcase.active_files %]
        <div class="publish-subcase-entity">

            <div class="publish-subcase-entity-title">
                [% pub_doc_count = 0 %]
                [% FOREACH file = active_files %]
                    [% NEXT IF(file.version != file.get_last_version.version) %]
                    [% NEXT IF(file.date_deleted || file.destroyed || !file.accepted) %]

                    [% IF file.has_label('publicatiedocument') %]
                        [% pub_doc_count = pub_doc_count + 1 %]
                    [% END %]
                [% END %]

                [% IF pub_doc_count == 0 %]
                <input type="checkbox" name="related_case_id" value="[% subcase.id %]" disabled="disabled" class="publish-subcase-action"/>
                <span class="title">Zaak [% subcase.id %]</span> |
                <span class="error">Zaak heeft geen publicatiedocument</span>
                [% ELSIF pub_doc_count == 1 %]
                <input type="checkbox" name="related_case_id" value="[% subcase.id %]" checked="checked" class="publish-subcase-action"/>
                <span class="title">Zaak [% subcase.id %]</span>
                [% ELSE %]
                <input type="checkbox" name="related_case_id" value="[% subcase.id %]" disabled="disabled" class="publish-subcase-action"/>
                <span class="title">Zaak [% subcase.id %]</span> |
                <span class="error">Zaak heeft meer dan 1 publicatiedocument</span>
                [% END %]
            </div>

            <div class="publish-subcase-entity-info">
                <ul>
                    <li>Onderwerp: [% subcase.value_by_magic_string({ magic_string => 'bbv_onderwerp', plain => 1 }) %]</li>
                    <li>Openbaarheid: [% subcase.value_by_magic_string({ magic_string => 'bbv_openbaarheid', plain => 1 }) %]</li>
                    <li>Embargodatum: [% subcase.value_by_magic_string({ magic_string => 'bbv_embargodatum', plain => 1 }) %]</li>
                </ul>
            </div>

            <div class="publish-subcase-entity-documents">
            [% FOREACH file = active_files %]
                [% NEXT IF (file.version != file.get_last_version.version) %]
                [% NEXT IF (file.date_deleted || file.destroyed || !file.accepted) %]
                <label>
                    <input type="checkbox" name="file_id" value="[% case.id %]-[% file.id %]"
                        [% IF file.has_label('publicatiedocument') %]
                            disabled="disabled"
                        [% END %]
                        [% IF file.publish_website %]checked="checked"[% END %]
                    /> [% file.name %][% file.extension %]
                    [% IF file.has_label %]
                        ([% file.labels.join(', ') %])
                    [% END %]
                </label>
                [% IF file.has_label('publicatiedocument') %]
                    <input type="hidden" name="file_id" value="[% case.id %]-[% file.id %]" />
                [% END %]
                <br/>
            [% END %]
            </div>

        </div>

    [% END %]
[% END %]


[% BLOCK vergaderingen_ede_documents %]
        [% WHILE (case = cases.next) %]
            <div class="publish-item">
                <div class="publish-meeting">
                    <h3 class="blue">Vergadering</h3>
                    <div>Zaak [% case.id %]<br/>
                        Datum/tijd: [% case.value_by_magic_string({ magic_string => 'vergader_datum' }) | xml %]
                        [% case.value_by_magic_string({ magic_string => 'vergader_tijdstip' }) | xml %]<br/>

                        Lokatie: [% case.value_by_magic_string({ magic_string => 'vergader_lokatie' }) | xml %]<br/>
                        Klasse: [% case.value_by_magic_string({ magic_string => 'vergader_klasse' }) | xml %]
                    </div>
                </div>
                <div class="publish-subcases">
                    <h3 class="blue">Te publiceren documenten</h3>
                    <div>[% PROCESS case_documents %]</div>
                </div>
            </div>


        [% END %]
        <small>Alle documenten worden gepubliceerd in PDF formaat.</small>
[% END %]


<div class="ezra_publish_form_wrapper">
[% IF commit %]

    <form class="ezra_publish_form_close">
        <p class="bericht">
        [% IF publish_result %]
            Er is iets misgegaan bij het publiceren.
            [% IF publish_error_description %]
                <div class="publish_error_description">
                [% publish_error_description %]
                </div>
            [% END %]
        [% ELSE %]
            Je selectie is gepubliceerd.
        [% END %]
        </p>
        <div>
            <a href="#" onclick="$(this).parent().find('div').toggle(); return false;">Details...</a>
            <div style="display:none;"><pre style="font-family: Courier New; white-space: pre-wrap;  /* css-3 */
 white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
 white-space: -pre-wrap;      /* Opera 4-6 */
 white-space: -o-pre-wrap;    /* Opera 7 */
 word-wrap: break-word;       /* Internet Explorer 5.5+ */
">[% publish_logging %]</pre></div>
        </div>
        <br/>
        <div class="form-actions">
        <input type="submit" value="Sluiten" class="button button-primary" />
        </div>
    </form>

[% ELSE %]


    [% config = profiles %]
    [% IF (config.keys.size || 0) == 0 %]
    <p>Er zijn geen publicatieprofielen actief.</p>
    [% ELSE %]
    <form action="/bulk/publish" class="ezra_publish_form" method="POST">
        <input type="hidden" name="selected_case_ids" value="[% selected_case_ids | html_entity %]"/>
        <input type="hidden" name="search_query_id" value="[% search_query_id | html_entity %]"/>
        <input type="hidden" name="selection" value="[% selection | html_entity %]"/>
        <input type="hidden" name="zql" value="[% zql | html_entity %]"/>

        [% profile = profile || config.keys.first %]
        <strong>Publicatieprofiel:</strong>
        <select name="profile" class="ezra_publication_profile">
            [% FOREACH key = config.keys %]
            <option value="[% key %]" [% IF profile == key %]selected="selected"[% END %]>[% key %]</option>
            [% END %]
        </select>
        <br/>
        <br/>
        <div class="spinner-groot"><div></div></div>
        [% IF config.$profile.internal_name.match('^vergaderingen_ede') %]
            [% PROCESS vergaderingen_ede_documents %]
        [% END %]
        <br/>
        <small><input type="checkbox" name="dry_run"/> Testmodus</small>
        <br/>
        <div class="form-actions">
            <input class="button button-primary" type="submit" name="publish" value="Publiceren"/>
        </div>
    </form>
    [% END %]

[% END %]
</div>
