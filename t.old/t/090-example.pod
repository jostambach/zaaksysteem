=head1 NAME

    t/090-example.pod - Example of test files for Zaaksysteem

=head1 DESCRIPTION

This document explains bits and pieces of our testsuite.
You should also have a look at the documents of L<Test::Class>.
This is our new way of testing.

=head1 Test::Class

    #! perl
    use TestSetup;

    ## Run the testsuite with Test::Class

    # Run all tests for General
    use Test::Class::Load 't/lib/TestFor/General';
    Test::Class->runtests();

    # Run all tests for Catalyst
    use Test::Class::Load 't/lib/TestFor/Catalyst';
    Test::Class->runtests();

    # Run a specific test
    ENV{TEST_METHOD} = 'name_of_test';

    # Run a subset of tests, this must be a regexp
    ENV{TEST_METHOD} = '.*multiple_tests.*';

    # The actual testclass module looks a bit like this:
    package TestFor::General::Casetype;
    use base qw(ZSTest);
    # Or, in case of Catalyst testing:
    use base qw(ZSTest::Catalyst);

    use TestSetup;

    # In case of Catalyst testing:
    sub my_cat_test : Test {
        my $mech = $zs->mech;
    }

    sub my_test : Test {
        ok(1, "this is ok");
    }

    sub my_tests : Tests {
        ok(1, "this is also ok");
        ok(2, "we also think this is ok");
    }


=head1 Old school style testing

    use TestSetup;
    initialize_test_globals_ok;

    $zs->zs_transaction_ok(
        sub {
            note 'Example';
        },
        'Test description'
    );

    zs_done_testing();

=head1 Testfiles

Because our move to Test::Class most of the order described here is going to be outdated.

=over

=item * 000 - 099

Initialization, very basic tests, like code compilation, POD syntax, example tests.

=item * 100 - 109

Utility modules

=item * 100-test-class.t

Test everthing from t/lib/TestFor/General

=item * 110 - 119

Backend::ResultSet

=item * 150 - 199

Backend::Sysin

=item * 200 - 250

FileStore, files

=item * 250 - 299

(unassigned)

=item * 300 - 399

Database objects (resultset, component)

=item * 400 - 499

(unassigned)

=item * 500 - 599

Controllers

=item * 600 - 649

ZAPI

=item * 650 - 699

SBUS

=item * 700 - 799

StUF

=item * 800 - 899

Catalyst tests

=item * t/800-catalyst.t

Test everything from t/lib/TestFor/Catalyst

Be aware that Catalyst does not respect our rollback mechanism, so you need to do your own housekeeping and clean up what you created.

=item * 990 - 999

Cleanup

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
