#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use JSON;

### Test header end
use_ok('Zaaksysteem::ZAPI::Form');
use_ok('Zaaksysteem::ZAPI::Form::Field');
use_ok('Zaaksysteem::ZAPI::Form::FieldSet');

my $json    = JSON->new;
$json->convert_blessed(1);

### Generate a form
{
    my @field_data = (
        {
            name            => 'type_subject',
            label           => 'Type subject',
            type            => 'select',
            description     => 'A small description',
            required        => 1,
        },
        {
            name            => 'subject',
            label           => 'Type subject',
            type            => 'text',
            description     => 'A small text',
            required        => 1,
        }
    );

    my @fields;
    push(
        @fields,
        Zaaksysteem::ZAPI::Form::Field->new(
            %$_
        )
    ) for @field_data;


    my $f       = Zaaksysteem::ZAPI::Form->new(
        name        => 'testname',
        fieldsets   => [
            Zaaksysteem::ZAPI::Form::FieldSet->new(
                name        => 'fieldset-gegevens',
                title       => 'Benodigde Gegevens',
                description => 'Lorem Ipsum',
                fields  => \@fields
            )
        ],
    );

    my $jsonp   = $json->utf8->decode($json->utf8->encode($f));

    is('testname', $jsonp->{name},'Correct form name');

    for (my $i = 0; $i < @field_data; $i++) {
        for my $key (keys %{ $field_data[$i] }) {
            my $value       = $field_data[$i]->{$key};
            my $formvalue   = $jsonp->{fieldsets}->[0]->{fields}->[$i]->{$key};
            next if ref $value;

            is (
                $value,
                $formvalue,
                'Found correct field value for: ' . $key
            );
        }
    }
}


### Check object constraints
{
    throws_ok(
        sub {
            my $f = Zaaksysteem::ZAPI::Form->new();
            $f->validate;
        },
        qr/Attribute.*name/,
        'Proper name validation of Form object'
    );
}

zs_done_testing();
