#! perl

### Test header start
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;
use JSON;

### Test header end
use_ok('Zaaksysteem::ZAPI::CRUD::Interface');
use_ok('Zaaksysteem::ZAPI::CRUD::Interface::Column');
use_ok('Zaaksysteem::ZAPI::CRUD::Interface::Action');

my $json    = JSON->new;
$json->convert_blessed(1);

### Generate a form
{
    my @column_data     = (
        {
            id              => 'external_transaction_id',
            label           => 'External ID',
            resolve         => 'external_transaction_id',
        },
        {
            id              => 'data_created',
            label       => 'datum/tijd',
            resolve     => 'date_created',
            sort        => {
                type        => 'alphanumeric',
                resolve     => 'date_created'
            }
        }
    );

    my @action_data     = (
        {
            id      => "case-delete",
            type    => "delete",
            label   => "Verwijderen"
        },
        {
            id      => "case-edit",
            type    => "popup",
            label   => "Wijzigen",
            data    => {
                url     => "/form/edit-case.html"
            },
            when    => "selectedItems.length==1"
        }
    );

    my @columns;
    push(
        @columns,
        Zaaksysteem::ZAPI::CRUD::Interface::Column->new(
            %$_
        )
    ) for @column_data;

    my @actions;
    push(
        @actions,
        Zaaksysteem::ZAPI::CRUD::Interface::Action->new(
            %$_
        )
    ) for @action_data;


    my $f       = Zaaksysteem::ZAPI::CRUD::Interface->new(
        columns     => \@columns,
        actions     => \@actions,
        url         => 'http://localhost/'
    );

    my $jsonp   = $json->utf8->decode($json->utf8->encode($f));

    ### Steekproef
    is($jsonp->{actions}->[0]->{id}, 'case-delete', 'Found action id');
    is($jsonp->{actions}->[1]->{type}, 'popup', 'Found action type');
    like($jsonp->{actions}->[1]->{data}->{url}, qr/edit-case.html/, 'Found action data.url');
    like($jsonp->{actions}->[1]->{when}, qr/selectedItems/, 'Found action when');

    is($jsonp->{columns}->[0]->{id}, 'external_transaction_id', 'Found column id');
    is($jsonp->{columns}->[0]->{label}, 'External ID', 'Found column label');
    is($jsonp->{columns}->[1]->{resolve}, 'date_created', 'Found column resolve');
    is($jsonp->{columns}->[1]->{sort}->{resolve}, 'date_created', 'Found column sort.resolve');
}



zs_done_testing();
