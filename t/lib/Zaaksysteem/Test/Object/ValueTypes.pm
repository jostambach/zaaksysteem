package Zaaksysteem::Test::Object::ValueTypes;

use Zaaksysteem::Test;

use Zaaksysteem::Object::Value;
use Zaaksysteem::Object::ValueType::Boolean;
use Zaaksysteem::Object::ValueType::Datetime;
use Zaaksysteem::Object::ValueType::ObjectRef;
use Zaaksysteem::Object::ValueType::String;
use Zaaksysteem::Object::ValueType::Text;

use DateTime;
use Zaaksysteem::Object::Reference;
use UUID4::Tiny qw[create_uuid_string];

sub test_value_type_boolean {
    my $type = Zaaksysteem::Object::ValueType::Boolean->new;

    my $true = Zaaksysteem::Object::Value->new(type => $type, value => 1);
    my $false = Zaaksysteem::Object::Value->new(type => $type, value => 0);

    ok !$type->equal($true, $false), 'true == false: negative boolean equality';
    ok $type->equal($true, $true), 'true == true: positive boolean equality';
    ok $type->not_equal($true, $false), 'true != false: positive boolean non-equality';
    ok !$type->not_equal($true, $true), 'true != true: negative boolean non-equality';
}

sub test_value_type_boolean_cast {
    my $boolean = Zaaksysteem::Object::ValueType::Boolean->new;
    my $string = Zaaksysteem::Object::ValueType::String->new;

    my $defined = Zaaksysteem::Object::Value->new(type => $string, value => 'abc');
    my $undefined = Zaaksysteem::Object::Value->new(type => $string);

    ok $boolean->equal($defined, $defined), 'string type defined == defined in equality context';
    ok !$boolean->equal($defined, $undefined), 'string type defined != undefined in equality context';
    ok $boolean->not_equal($defined, $undefined), 'string type defined != undefined in non-equality context';
    ok !$boolean->not_equal($defined, $defined), 'string type defined != defined in non-equality context';
}

sub test_value_type_datetime {
    my $type = Zaaksysteem::Object::ValueType::Datetime->new;

    my $now = Zaaksysteem::Object::Value->new(
        type => $type,
        value => DateTime->now
    );

    my $yesterday = Zaaksysteem::Object::Value->new(
        type => $type,
        value => DateTime->now->subtract(days => 1)
    );

    ok $type->equal($now, $now), 'now == now: positive equality';
    ok !$type->equal($now, $yesterday), 'now != now: negative equality';
    ok $type->not_equal($now, $yesterday), 'now != yesterday: positive non-equality';
    ok !$type->not_equal($now, $now), 'now != now: negative non-equality';

    ok $type->greater_than($now, $yesterday), 'now > yesterday: positive greater-than';
    ok !$type->greater_than($yesterday, $now), 'now > yesterday: negative greather-than';
    ok !$type->greater_than($now, $now), 'now > now: negative greater-than';

    ok $type->less_than($yesterday, $now), 'yesterday < now: positive less-than';
    ok !$type->less_than($now, $yesterday), 'yesterday < now: negative less-than';
    ok !$type->less_than($now, $now), 'now < now: negative less-than';

    ok $type->equal_or_greater_than($now, $yesterday), 'now >= yesterday: postive equal-or-greater-than';
    ok !$type->equal_or_greater_than($yesterday, $now), 'yesterday >= now: negative equal-or-greater-than';
    ok $type->equal_or_greater_than($now, $now), 'now >= now: positive equal-or-greater-than';

    ok $type->equal_or_less_than($yesterday, $now), 'yesterday <= now: positive equal-or-less-than';
    ok !$type->equal_or_less_than($now, $yesterday), 'now <= yesterday: negative equal-or-less-than';
    ok $type->equal_or_less_than($now, $now), 'now <= now: positive equal-or-less-than';
}

sub test_value_type_datetime_cast {
    my $datetime = Zaaksysteem::Object::ValueType::Datetime->new;
    my $string = Zaaksysteem::Object::ValueType::String->new;

    my $now = DateTime->now;

    my $nop_string = Zaaksysteem::Object::Value->new(
        type => $string,
        value => ''
    );

    my $now_string = Zaaksysteem::Object::Value->new(
        type => $string,
        value => $now->iso8601
    );

    my $now_datetime = Zaaksysteem::Object::Value->new(
        type => $datetime,
        value => $now
    );

    my $test = $datetime->as_datetime($now_string);

    isa_ok $test, 'DateTime', 'string->datetime casting returns value';

    ok !$test->compare($now), 'string->datetime casting value equals original DateTime';

    my $nop_test = $datetime->as_datetime($nop_string);

    ok !defined $nop_test, 'string->datetime casting returns undefined for non-datetimes';
}

sub test_value_type_object_ref {
    my $type = Zaaksysteem::Object::ValueType::ObjectRef->new;

    my $a = Zaaksysteem::Object::Value->new(
        type => $type,
        value => Zaaksysteem::Object::Reference::Instance->new(
            id => create_uuid_string(),
            type => 'foo'
        )
    );

    my $b = Zaaksysteem::Object::Value->new(
        type => $type,
        value => Zaaksysteem::Object::Reference::Instance->new(
            id => create_uuid_string(),
            type => 'foo'
        )
    );

    ok $type->equal($a, $a), 'a == a: positive equality';
    ok !$type->equal($a, $b), 'a == b: negative equality';
    ok !$type->not_equal($a, $a), 'a != a: negative non-equality';
    ok $type->not_equal($a, $b), 'a != b: positive non-equality';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
